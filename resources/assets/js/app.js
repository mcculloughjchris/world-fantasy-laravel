
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

var axios = require('axios');
window.Vue = require('vue');

var lastScroll = $(window).scrollTop(),
	$navBar = '';

$(document).ready(function(){
	$navBar = $('.navbar');
    $('[data-toggle="tooltip"]').tooltip();   
});

$(window).scroll(function (e) {
	if ($(this).scrollTop() > lastScroll) {
		$navBar.addClass('scrolled');
	} else {
		$navBar.removeClass('scrolled');
	}
	lastScroll = $(this).scrollTop();
});

$(window).mousemove(function (e) {
	if (e.originalEvent.clientY < 50) {
		$navBar.removeClass('scrolled');
	}
});

export const EventBus = new Vue();

require('./components/list-filter.js');
require('./components/tag-selector.js');
require('./components/voting-component.js');
require('./components/notifications-component.js');
require('./components/friend-button.js');
require('./components/friend.js');
require('./components/friend-request.js');
require('./components/friend-sent.js');
require('./components/recent-stories-to-players.js');
require('./components/top-leagues-top-teams-top-players.js');

const vue = new Vue({
	el: '#vue'
});