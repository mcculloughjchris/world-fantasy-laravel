Vue.component('recent-stories-to-players', {
	data: function () {
		return {
			stories: {},
			pointnames: {},
			people: {}
		}
	},
	props: ['storiesjson', 'pointtypes'],
	mounted: function () {
		this.stories = JSON.parse(this.storiesjson);
		this.pointnames = JSON.parse(this.pointtypes);
	},
	methods: {
		storyToPlayers: function (e) {
			var target,
				storyID;

			if (e.target.tagName === 'TR') {
				target = e.target;
			} else if (e.target.tagName === 'TD') {
				target = e.target.parentNode;
			} else if (e.target.tagName === 'A') {
				target = e.target.parentNode.parentNode;
			}

			storyID = parseInt(target.getAttribute('data-id'), 10);

			var _ = this;

			axios.get('/api/homepage/story-to-players/' + storyID).then(function (results) {
				_.people = results.data;
				console.log(results.data);
			});
		},
		tallyPoints: function (points) {
			var total = 0;

			for (var i = 0; i < points.length; i += 1) {
				total += points[i].direction;
			}

			return total;
		}
	},
	template: `
		<div class="row">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">Recent Stories</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Story...</th>
										<th>Date</th>
										<th>Source</th>
									</tr>
								</thead>
								<tbody>
									<template v-for="story in stories">
										<tr v-on:click="storyToPlayers" :data-id="story.id">
											<td><a :href="'/stories/' + story.id">{{story.title}}</a></td>
											<td>{{story.date}}</td>
											<td><a :href="story.url" target="_blank">{{story.url}}</a></td>
										</tr>
									</template>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">Gives Points To...</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table id="story-statistics" class="table">
								<thead>
									<tr>
										<th>Name</th>
										<template v-for="pointname in pointnames">
											<th>{{pointname.name}}</th>
										</template>
									</tr>
								</thead>
								<tbody>
									<template v-for="person in people">
										<tr>
											<td>{{person.person.name}}</td>
											<template v-for="pointsname in pointnames">
												<td v-if="person.points.points[pointsname.name].length !== 0">
													{{ tallyPoints(person.points.points[pointsname.name]) }}
												</td>
												<td v-else>0</td>
											</template>
										</tr>
									</template>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
});