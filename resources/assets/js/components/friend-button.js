Vue.component('friend-button', {
	data: function() {
		var heading,
			body;

		if (this.type === 'add') {
			body = 'Are you sure you want to request ' + this.username + ' as a friend?';
			heading = 'Request ' + this.username + ' as a friend?';
		}

		return {
			heading: heading,
			body: body,
			alertClass: ''
		}
	},
	props: ['type', 'username', 'uid'],
	methods: {
		button_clickHandler: function (e) {
			var _ = this;
			if (this.type === 'add') {
				axios.get('/api/friendship/?action=add&to=' + _.uid).then(function (response) {
					if (response.data !== undefined) {
						if (response.data.msg !== undefined) {
							_.body = response.data.msg;
							_.alertClass = 'alert alert-' + response.data.type;
						}
					}
				});
			}
		}
	},
	template: `
		<div class="friend-button">
			<a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#add"><span class="glyphicon glyphicon-plus-sign"></span></a>
			<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="addLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Request {{username}} as a friend?</h4>
						</div>
						<div class="modal-body"><div :class="alertClass">{{body}}</div></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" v-on:click="button_clickHandler">
								<template v-if="type === 'add'">
									Request
								</template>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
});