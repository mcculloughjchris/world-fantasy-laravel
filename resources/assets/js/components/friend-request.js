Vue.component('friend-request', {
	data: function () {
		return {
			headerMsg: '',
			bodyMsg: '',
			buttonAction: '',
			visiblity: 'show'
		}
	},
	props: ['id', 'name'],
	methods: {
		openDeny_clickHandler: function (e) {
			this.headerMsg = 'Accept ' + this.name + '\'s friendship request?';
			this.bodyMsg = 'Are you sure you want to accept ' + this.name + '\'s friendship request?';
			this.buttonAction = this.deny;
		},
		deny: function (e) {
			var _ = this;
			axios.get('/api/friendship?action=deny&to=' + this.id).then(function (response) {
				if (response.data.success !== undefined) {
					_.visiblity = 'hidden';
				}
			});
		},
		openAccept_clickHandler: function (e) {
			this.headerMsg = 'Deny ' + this.name + '\'s friendship request?';
			this.bodyMsg = 'Are you sure you want to deny ' + this.name + '\'s friendship request?';
			this.buttonAction = this.accept;
		},
		accept: function (e) {
			var _ = this;
			axios.get('/api/friendship?action=accept&to=' + this.id).then(function (response) {
				if (response.data.success !== undefined) {
					_.visiblity = 'hidden';
				}
			});
		}
	},
	template: `
		<li :class="'list-group-item ' + visiblity">
			{{name}}
			<div class="pull-right">
				<button class="btn btn-primary btn-xs" v-on:click="openDeny_clickHandler" data-toggle="modal" :data-target="'#currentFriend-' + id"><span class="glyphicon glyphicon-remove"></span></button>
				<button class="btn btn-primary btn-xs" v-on:click="openAccept_clickHandler" data-toggle="modal" :data-target="'#currentFriend-' + id"><span class="glyphicon glyphicon-ok"></span></button>
			</div>
			<div class="modal fade" :id="'currentFriend-' + id" tabindex="-1" role="dialog" aria-labelledby="addLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">{{headerMsg}}</h4>
						</div>
						<div class="modal-body">{{bodyMsg}}</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal" v-on:click="buttonAction">Remove</button>
						</div>
					</div>
				</div>
			</div>
		</li>
	`
});