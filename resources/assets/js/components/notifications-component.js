Vue.component('notificationscomponent', {
	data: function () {
		return {
			notifications: ''
		};
	},
	mounted: function () {
		var _ = this;
		axios.get('/api/notifications').then(function (response) {
			if (response.data.length !== 0) {
				_.notifications = response.data;
				return true;
			}

			_.notifications = '<li><i>No notifications right now</i></li>';
		});
	},
	methods: {
		handleHover: function (e) {
			if (e.target.className !== 'new') {
				return false;
			}

			var id = parseInt(e.target.getAttribute('data-id'), 10);

			axios.get('/api/notifications/see?id=' + id).then(function (response) {
				if (response.data === 1) {
					e.target.className = '';
				}
			});
		},
		handleClick: function (e) {
			if (e.target.className !== 'glyphicon glyphicon-remove') {
				return false;
			}

			var id = parseInt(e.target.parentNode.getAttribute('data-id'), 10);

			axios.get('/api/notifications/delete?id=' + id).then(function (response) {
				if (response.data === 1) {
					e.target.parentNode.remove();
				}
			});
		}
	},
	template: `
		<ul class="dropdown-menu notifications" v-html="notifications" v-on:mouseover="handleHover" v-on:click="handleClick">
		</ul>
	`
});
