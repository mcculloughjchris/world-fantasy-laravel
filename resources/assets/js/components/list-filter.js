/**
 * This filters a list as the input value updates
 * Hides and displays a dropdown (if available) when necessary
 */


Vue.component('list-filter', {
	/**
	 * Data that this vue component will use
	 */
	data: function () {
		return {
			searchingFor: '',
			visibility: 'hidden',
			displayedOptions: []
		};
	},
	/**
	 * Properties this vue component needs
	 */
	props: ['label', 'options', 'clickcallback'],
	/**
	 * When the component loads this function will run
	 */
	mounted: function () {
		/**
		 * put `this` in a variable so we can use it later
		 */
		var _ = this;
		/**
		 * When the 'hide-dropdown' event is published:
		 */
		EventBus.$on('hide-dropdown', function () {
			/**
			 * Hide the dropdown
			 */
			_.hideDropdown();
		});
		/**
		 * When the 'display-dropdown' event is published:
		 */
		EventBus.$on('display-dropdown', function () {
			/**
			 * Display the dropdown
			 */
			_.displayDropdown();
		});
	},
	/**
	 * Methods this component will use
	 */
	methods: {
		/**
		 * This function display the dropdown
		 */
		displayDropdown: function () {
			this.visibility = 'show';
		},
		/**
		 * This function hides the dropdown
		 */
		hideDropdown: function (e) {
			this.visibility = 'hidden';
		},
		/**
		 * This function filters through the list
		 */
		filter: function (text) {
			/**
			 * Check if the length of the text being searched for is 0
			 */
			if (text.length === 0) {
				/**
				 * If it is, hide the dropdown and end the function
				 */
				this.hideDropdown();
				return false;
			}

			/**
			 * `opts` will be used to temporarily store all the matching values
			 */
			var opts = [];

			/**
			 * Loop through all the available options
			 */
			for (var i = 0; i < this.options.length; i += 1) {
				/**
				 * Check to see if the word being searched for is within this option
				 */
				if (this.options[i].value.toLowerCase().indexOf(text) !== -1) {
					/**
					 * if it is, add it to `opts`
					 */
					opts.push(this.options[i]);
				}
			}

			/**
			 * Check if there are no results
			 */
			if (opts.length === 0) {
				/**
				 * If there are no results hide the dropdown and stop the function
				 */
				this.hideDropdown();
				return false;
			}

			/**
			 * Replace the content of the dropdown with the results
			 * Display the dropdown
			 */
			this.displayedOptions = opts;
			this.displayDropdown();
		}
	},
	/**
	 * If the variables in `watch` change, the following function runs
	 */
	watch: {
		/**
		 * `options` variable watch function
		 */
		options: function () {
			/**
			 * if the available options change, make them visible in the dropdown
			 */
			this.displayedOptions = this.options;
		},
		/**
		 * `searchingFor` variable watch function
		 */
		searchingFor: function () {
			/**
			 * run the filter function with the new string
			 */
			this.filter(this.searchingFor.toLowerCase());
		}
	},
	/**
	 * Template for list-filter component
	 */
	template: `
		<div class="form-group" v-on:keyup.esc="hideDropdown">
			<label :for="label">{{label}}</label>
			<input type="text"  class="form-control" v-model="searchingFor" v-on:focus="displayDropdown" />
			<div :class="'tag-selector-options ' + visibility">
				<template v-for="option in this.displayedOptions">
					<div :data-id="option.id" :data-value="option.value" v-on:click="clickcallback ">
						{{option.value}}
					</div>
				</template>
			</div>
		</div>
	`,
});