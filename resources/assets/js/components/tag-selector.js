Vue.component('tagselector', {
	data: function () {
		return {
			options: [],
			selectedOptions: [],
			displayedOptions: [],
			searchingFor: '',
			visibility: 'hidden'
		};
	},
	props: ['api', 'input_name', 'label', 'max'],
	mounted: function () {
		var _ = this;
		axios.get(this.api).then(function (response) {
			_.options = response.data;
			_.displayedOptions = _.options;
		});
	},
	methods: {
		selectOption: function (event) {
			var target = event.target,
				option = {};

			option.id = target.getAttribute('data-id');
			option.value = target.getAttribute('data-value');

			if (option.id !== '' && option.value !== '') {
				if (this.max !== undefined) {
					if (this.selectedOptions.length === parseInt(this.max, 10)) {
						return false;
					}
				}

				for (var i = 0; i < this.selectedOptions.length; i += 1) {
					if (this.selectedOptions[i].id === option.id) {
						return false;
					}
				}

				this.selectedOptions.push({
					'id': option.id,
					'value': option.value
				});

				this.searchingFor = '';
				EventBus.$emit('hide-dropdown');
				return true;
			}
			return false;
		},
		deselectOption: function (event) {
			var target = event.target;
			if (target.tagName === 'SPAN') {
				target = target.parentNode;
			}

			var id = target.getAttribute('data-id');

			for (var i = 0; i < this.selectedOptions.length; i += 1) {
				if (this.selectedOptions[i].id === id) {
					this.selectedOptions.splice(i, 1);
					return true;
				}
			}

			return false;
		},
		displayDropdown: function () {
			this.visibility = 'show';
		},
		hideDropdown: function (e) {
			this.visibility = 'hidden';
		}
	},
	watch: {
		selectedOptions: function () {
			document.getElementById(this.input_name).value = JSON.stringify(this.selectedOptions);
		},
		searchingFor: function () {
			this.filter(this.searchingFor.toLowerCase());
		}
	},
	template: `
		<div class="tag-selector">
			<list-filter :label="label" :options="options" :clickcallback="this.selectOption"></list-filter>
			<div class="selected-people tags">
				<span class="tag" v-for="option in this.selectedOptions">
					{{option.value}}
					<a href="#" v-on:click="deselectOption" :data-id="option.id">
						<span class="glyphicon glyphicon-remove-sign"></span>
					</a>
				</span>
			</div>
			<input type="hidden" :id="input_name" :name="input_name" value="[]" />
		</div>
	`
});