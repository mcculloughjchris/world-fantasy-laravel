Vue.component('top-leagues-top-teams-top-players', {
	data: function () {
		return {
			leaguelist: {}
		}
	},
	props: ['leagues'],
	mounted: function () {
		this.leaguelist = JSON.parse(this.leagues);
	},
	template: `
		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">Top Leagues</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>League</th>
										<th>Points</th>
									</tr>
								</thead>
								<tbody>
									<template v-for="league in leaguelist">
										<tr>
											<td>{{ league.league_name }}</td>
											<td>{{ league.points }}</td>
										</tr>
									</template>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">Top Teams</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">Top Players</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
});