Vue.component('votingcomponent', {
	props: [ "personid", "storyid", "pointtype", "points" ],
	data: function () {
		return {
			'displayPoints': this.points
		}
	},
	methods: {
		vote: function (type) {
			var _ = this;
			var direction = (type === 'up') ? 1 : -1;
			axios.get('/api/vote/' + this.storyid + '/' + this.personid + '/' + this.pointtype + '/' + direction).then(function (response) {
				_.displayPoints = response.data;
			});
		},
		voteUp: function (e) {
			e.preventDefault();
			this.vote('up');
		},
		voteDown: function (e) {
			e.preventDefault();
			this.vote('down');
		}
	},
	template: `<div class="voting-component" data-toggle="tooltip" title="Click the up and down arrows to vote in this category">
		<div class="voting-options">
			<a href="#" class="down" v-on:click="voteDown">
				<span class="glyphicon glyphicon-arrow-down"></span>
			</a>
			<a href="#" class="up" v-on:click="voteUp">
				<span class="glyphicon glyphicon-arrow-up"></span>
			</a>
		</div>
		<span class="points">
			{{ displayPoints }}
		</span>
	</div>`
});