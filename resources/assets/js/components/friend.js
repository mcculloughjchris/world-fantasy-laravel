Vue.component('friend', {
	data: function () {
		return {
			'visiblity': 'show'
		}
	},
	props: ['id', 'name'],
	methods: {
		remove_clickHandler: function () {
			var _ = this;
			axios.get('/api/friendship?action=remove&to=' + this.id).then(function () {
				_.visiblity = 'hidden';
			});
		}
	},
	template: `
		<li :class="'list-group-item ' + visiblity">
			{{name}}
			<div class="pull-right">
				<button class="btn btn-primary btn-xs" data-toggle="modal" :data-target="'#remove-' + id"><span class="glyphicon glyphicon-remove"></span></button>
			</div>
			<div class="modal fade" :id="'remove-' + id" tabindex="-1" role="dialog" aria-labelledby="addLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Remove {{name}} as a friend?</h4>
						</div>
						<div class="modal-body">Are you sure you want to remove {{name}} as a friend?</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal" v-on:click="remove_clickHandler">Remove</button>
						</div>
					</div>
				</div>
			</div>
		</li>
	`
});