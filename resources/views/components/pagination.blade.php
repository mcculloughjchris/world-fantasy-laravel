@php

$page = Request::get('page');
	$page = (intval($page) === 0) ? 1 : intval($page);

@endphp

@if ($pages !== 0)
	<div class="pagination">
		@for ($i = 0; $i < $pages; $i += 1)
			<a href="/{{$url}}?q={{$q}}&page={{$i + 1}}" class="btn btn-default
			@if ($page === ($i + 1))
				disabled
			@endif
			">{{$i + 1}}</a>
		@endfor
	</div>
@endif