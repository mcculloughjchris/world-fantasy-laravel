@extends('layouts.master')

@section('title', 'Fantasy Articles - My Teams')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">My Teams <a href="/my-teams/create" class="pull-right"><span class="glyphicon glyphicon-plus-sign"></span></a></div>
		<div class="panel-body">
			<table class="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Players</th>
						<th>Joined Leagues</th>
						<th>Created On</th>
					</tr>
				</thead>
				<tbody>

					@if (count($teams))

						@foreach ($teams as $k => $v)
							<tr>
								<td>{{$v['team']->name}}</td>
								<td>
									<div class="tags">
										@php
										foreach ($v['players'] as $p) {
											echo PersonController::tag($p->person_id);
										}
										@endphp
									</div>
								</td>
								<td>
									<div class="tags">
										@php
										foreach ($v['leagues'] as $l) {
											echo LeagueController::tag($l->id);
										}
										@endphp
									</div>
								</td>
								<td>{{DateController::str($v['team']->datestamp)}}</td>
							</tr>
						@endforeach
						
					@else
						<tr>
							<td>No results</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>

@endsection