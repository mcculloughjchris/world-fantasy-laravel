@extends('layouts.master')

@section('title', 'Fantasy Articles - Leagues')

@section('content')

	@if (isset($league))

		@php
		$teams = $league->teams;

		$myTeams = Auth::user()->getTeams();
		$myTeamsIds = [];

		$over = time() >= $league->runs_to;

		foreach ($myTeams as $t) {
			$myTeamsIds[] = $t->id;
		}

		$coverPhotoUrl = LeagueController::getCoverPhotoUrl($league->id);
		$coverPhotoClass = ($coverPhotoUrl) ? "move-up" : "";
		@endphp

		@if ($coverPhotoUrl)
			<div class="cover-photo">
				<img src="{{ $coverPhotoUrl }}" />
			</div>
		@endif

		<div class="panel panel-default {{$coverPhotoClass}}">
			<div class="panel-heading profile-heading">
				@if (LeagueController::getAvatarUrl($league->id))
					<img src="{{ LeagueController::getAvatarUrl($league->id) }}" class="profile-picture" />
				@endif
				@if ($league->private === 1)
					<span class="glyphicon glyphicon-lock"></span>
				@endif
				{{$league->name}}
				<div class="pull-right panel-heading-actions">
					@if ($league->userJoined === 0)
						<a href="/leagues/join/{{$league->id}}" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Join League"><span class="glyphicon glyphicon-knight"></span></a>
					@endif
					@if ($league->user_id === Auth::user()->id)
						<span data-toggle="tooltip" title="Invite users to this league">
							<a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-share"></span></a>
						</span>
						<a href="/leagues/edit/{{$league->id}}" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit League"><span class="glyphicon glyphicon-cog"></span></a>
					@endif
				</div>
			</div>
			<div class="panel-body">

				@if ($over)
					<p><i>This league ended on {{DateController::str($league->runs_to)}}</i></p>
				@else
					<p>This league ends on {{DateController::str($league->runs_to)}}</p>
				@endif

				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Players</th>
							<th>Score</th>
							<th>Created On</th>
							@if ($over)
								<th></th>
							@endif
						</tr>
					</thead>
					<tbody>

						@if (count($teams))

							@php
							$o = 0;
							@endphp

							@foreach ($teams as $k => $v)
								<tr>
									<td>{{$v['team']->name}}</td>
									<td>
										<div class="tags">
											@for ($i = 0; $i < count($v['players']); $i += 1)
												<span class="tag">
													{{$v['players'][$i]->name}}
													<a href="/players/{{$v['players'][$i]->id}}">
														<span class="glyphicon glyphicon-eye-open"></span>
													</a>
												</span>
											@endfor
										</div>
									</td>
									<td>{{ TeamsController::getScore($v['team']->id, $league->id) }}</td>
									<td>{{DateController::str($v['team']->datestamp)}}</td>
									@php

									if ($over) {
										if ($o === 0) {
											echo "<td><span class='glyphicon glyphicon-king'></span></td>";
										} else {
											echo "<td></td>";
										}
									}

									$o += 1;

									@endphp
								</tr>
							@endforeach

						@else
							<tr>
								<td>No results</td>
							</tr>
						@endif
					</tbody>
				</table>

				<!-- Modal -->
				@php
				$data = Auth::user()->friendship();
				$friends = call_user_func($data->friends);

				$options = [];

				for ($i = 0; $i < count($friends); $i += 1) {
					$friend = ProfileController::getUserData($friends[$i]);
					$options[$friend->id] = $friend->name;
				}
				@endphp
			</div>
		</div>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Invite Users</h4>
					</div>
		          	{{ Form::open(array('url' => '/leagues/invite', 'method' => 'post')) }}
			      		<div class="modal-body">
			        		<input type="hidden" name="league_id" value="{{$league->id}}" />
			        		<tagselector api="/api/friends-list/{{Auth::user()->id}}" input_name="invited" label="Friends"></tagselector>
						</div>
			      		<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        		<button type="submit" class="btn btn-primary">Invite</button>
						</div>
					{{ Form::close() }}
			    </div>
			</div>
		</div>
	@else

		@php

		$sortURL = '/leagues?';

		if (Request::get('q')) {
			$sortURL .= 'q=' . Request::get('q') . '&';
		}

		$options = array (
			'baseURL' => $sortURL,
			'by' => Request::get('by'),
			'sort' => Request::get('sort'),
			'columns' => array (
				array (
					'name' => 'name',
					'glyph' => 'alphabet'
				),
				array (
					'name' => 'runs_from',
					'glyph' => 'order'
				),
				array (
					'name' => 'runs_to',
					'glyph' => 'order'
				),
				array (
					'name' => 'max_teams',
					'glyph' => 'order'
				),
				array (
					'name' => 'wager',
					'glyph' => 'order'
				)

			)
		);
		$sorting = App\Http\Controllers\OrderingController::build($options);

		@endphp

		<div class="panel panel-default">

			<div class="panel-heading">
				Leagues
				<div class="pull-right">
					<a href="/leagues/create" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Start League"><span class="glyphicon glyphicon-plus-sign"></span></a>
				</div>
			</div>

			<div class="panel-body">

				{{ Form::open(array('url' => 'leagues', 'method' => 'get')) }}
					<div class="form-group">
						{{ Form::text('q', '', array('placeholder' => 'Search', 'class' => 'form-control')) }}
					</div>

					{{ Form::submit('Search', array('class' => 'btn btn-default')) }}

					@if (Request::get('q') || (Request::get('sort') && Request::get('by')))
						<a href="/leagues" class="btn btn-danger pull-right">Reset</a>
					@endif
				{{ Form::close() }}

				<hr />

				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th></th>
								<th><a href="{{ $sorting['name']['url'] }}"><span class="{{ $sorting['name']['glyph'] }}"></span></a>&nbsp;League</th>
								<th><a href="{{ $sorting['runs_from']['url'] }}"><span class="{{ $sorting['runs_from']['glyph'] }}"></span></a>&nbsp;Starts</th>
								<th><a href="{{ $sorting['runs_to']['url'] }}"><span class="{{ $sorting['runs_to']['glyph'] }}"></span></a>&nbsp;Ends</th>
								<th><a href="{{ $sorting['max_teams']['url'] }}"><span class="{{ $sorting['max_teams']['glyph'] }}"></span></a>&nbsp;# Of Teams</th>
								<th><a href="{{ $sorting['wager']['url'] }}"><span class="{{ $sorting['wager']['url'] }}"></span></a>&nbsp;Wagering</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@if (count($leagues) > 0)
								@for ($i = 0; $i < count($leagues); $i += 1)
									<tr>
										<td style="text-align:center">
											@if ($leagues[$i]->private === 1)
												<span class="glyphicon glyphicon-lock" data-toggle="tooltip" title="Private League"></span>
											@endif
										</td>
										<td>
											<a href="/leagues/{{str_slug($leagues[$i]->name)}}">{{$leagues[$i]->name}}</a>
										</td>
										<td>{{DateController::str($leagues[$i]->runs_from)}}</td>
										<td>{{DateController::str($leagues[$i]->runs_to)}}</td>
										<td>{{count($leagues[$i]->teams)}}/{{$leagues[$i]->max_teams}}</td>
										<td>{{$leagues[$i]->wager}}</td>
										<td align="right">
											@if ($leagues[$i]->private === 1)
											@else
												@if (!isset($leagues[$i]->userJoined) || $leagues[$i]->userJoined === 0)
													<a href="/leagues/join/{{$leagues[$i]->id}}" class="btn btn-primary" data-toggle="tooltip" title="Join League"><span class="glyphicon glyphicon-knight"></span></a>
												@else
													<a href="#" class="btn btn-primary disabled" data-toggle="tooltip" title="You already joined this league"><span class="glyphicon glyphicon-knight"></span></a>
												@endif
											@endif
										</td>
									</tr>
								@endfor
							@else
								<tr>
									<td>No leagues available</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>

			{!! $pagination !!}

			</div>
		</div>
	@endif

@endsection
