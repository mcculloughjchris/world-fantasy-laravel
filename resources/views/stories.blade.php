@extends('layouts.master')

@section('title', 'Fantasy Articles - Stories')

@section('content')

	@if (isset($story))

		<div>
			<div class="jumbotron">
				<img src="{{$story->img_url}}" width="200" class="pull-left" style="margin-right:20px" />
				<h3>{{$story->title}}</h3>
				<p><a href="{{$story->url}}" target="_blank">{{$story->url}}</a></p>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">People</div>
				<div class="panel-body">
					@if (count($people))
						<div class="table-responsive">
							<table class="table">
								<thead>
									<th>Person</th>
									@for ($i = 0; $i < count($pointTypes); $i += 1)
										<th>{{$pointTypes[$i]->name}}</th>
									@endfor
								</thead>
								<tbody>
									@for ($i = 0; $i < count($people); $i += 1)
										{!! $people[$i]['pointsDataRow'] !!}
									@endfor
								</tbody>
							</table>
						</div>
					@else
					@endif
				</div>
			</div>
		</div>

	@else

		@php
		$sortURL = '/stories?';
		if (Request::get('q')) {
			$sortURL .= 'q=' . Request::get('q') . '&';
		}


		$options = array (
			'baseURL' => $sortURL,
			'by' => Request::get('by'),
			'sort' => Request::get('sort'),
			'columns' => array (
				array (
					'name' => 'title',
					'glyph' => 'alphabet'
				),
				array (
					'name' => 'datestamp',
					'glyph' => 'order'
				),
				array (
					'name' => 'url',
					'glyph' => 'alphabet'
				)
			)
		);
		$sorting = App\Http\Controllers\OrderingController::build($options);

		@endphp

		<div class="panel panel-default">
			<div class="panel-heading">Stories</div>
			<div class="panel-body">
				{{ Form::open(array('url' => 'stories', 'method' => 'get')) }}
					<div class="form-group">
						{{ Form::text('q', '', array('placeholder' => 'Search', 'class' => 'form-control')) }}
					</div>

					{{ Form::submit('Search', array('class' => 'btn btn-default')) }}

					@if (Request::get('q') || (Request::get('sort') && Request::get('by')))
						<a href="/stories" class="btn btn-danger pull-right">Reset</a>
					@endif
				{{ Form::close() }}

				<hr />

				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th><a href="{{ $sorting['title']['url'] }}"><span class="{{ $sorting['title']['glyph'] }}"></span></a> Story</th>
								<th><a href="{{ $sorting['datestamp']['url'] }}"><span class="{{ $sorting['datestamp']['glyph'] }}"></span></a> Date</th>
								<th><a href="{{ $sorting['url']['url'] }}"><span class="{{ $sorting['url']['glyph'] }}"></span></a> Source</th>
							</tr>
						</thead>
						<tbody>
							@if (count($stories) > 0)
								@for ($i = 0; $i < count($stories); $i += 1)
									<tr>
										<td><a href="/stories/{{ $stories[$i]->id }}">{{ $stories[$i]->title }}</a></td>
										<td>{{DateController::str($stories[$i]->datestamp)}}</td>
										<td><a href="{{$stories[$i]->url}}" target="_blank">{{$stories[$i]->url}} <span class="glyphicon glyphicon-link"></span></a></td>
									</tr>
								@endfor
							@else
								<tr>
									<td>No stories available</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>

				{!! $pagination !!}
			</div>
		</div>

	@endif


@endsection
