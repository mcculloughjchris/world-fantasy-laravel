@php

$data = Auth::user()->friendship();
$requests = call_user_func($data->requests);
$friends = call_user_func($data->friends);
$sent = call_user_func($data->sent);

@endphp

@extends('layouts.master')

@section('title', 'Fantasy Articles')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">Incoming Friend Requests</div>
		<div class="panel-body">
			@if (count($requests) !== 0)
				<ul class="list-group">
					@for ($i = 0; $i < count($requests); $i += 1)
						@php
						$user = ProfileController::getUserData($requests[$i]);
						@endphp
						<friend-request id="{{$user->id}}" name="{{$user->name}}"></friend-request>
					@endfor
				</ul>
			@else
				<i>You have no friend requests right now.</i>
			@endif
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">Friends</div>
		<div class="panel-body">
			@if (count($friends) !== 0)
				<ul class="list-group">
					@for ($i = 0; $i < count($friends); $i += 1)
						@php
						$user = ProfileController::getUserData($friends[$i]);
						@endphp
						<friend id="{{$user->id}}" name="{{$user->name}}"></friend>
					@endfor
				</ul>
			@else
				<i>You have no friends right now.</i>
			@endif
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">Outgoing Friend Requests</div>
		<div class="panel-body">
			@if (count($sent) !== 0)
				<ul class="list-group">
					@for ($i = 0; $i < count($sent); $i += 1)
						@php
						$user = ProfileController::getUserData($sent[$i]);
						@endphp
						<friend-sent id="{{$user->id}}" name="{{$user->name}}"></friend-sent>
					@endfor
				</ul>
			@else
				<i>You have no sent friend requests right now.</i>
			@endif
		</div>
	</div>

@endsection