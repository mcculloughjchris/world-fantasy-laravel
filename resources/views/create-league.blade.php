@php

$name = '';

if (isset($id)) {
	$league = LeagueController::get($id);
	$name = $league->name;
}

@endphp

@extends('layouts.master')

@section('title', 'Fantasy Articles - Create League')

@section('content')

	<div class="panel panel-default">

		<div class="panel-body">

			{{ Form::open(array('url' => '/leagues/create', 'method' => 'post', 'enctype' => 'multipart/form-data')) }}

				@foreach ($errors->all() as $error)
					<div class="alert alert-danger">{{$error}}</div>
				@endforeach

				<div class="alert alert-warning">You cannot change these settings after saving.</div>

				<div class="form-group">
					{{ Form::label('name', 'Name') }}
					{{ Form::text('name', $name, array('class' => 'form-control', 'placeholder' => 'Name')) }}
				</div>

				<div class="form-group">
					@if (isset($id))
						@if (LeagueController::getAvatarUrl($id))
							<img src="{{ LeagueController::getAvatarUrl($id) }}" width="60" class="pull-left" style="margin-right:8px" />
						@endif
					@endif
					{{ Form::label('avatar', 'Avatar') }}
					{{ Form::file('avatar') }}
				</div>

				<div class="form-group">
					@if (isset($id))
						@if (LeagueController::getCoverPhotoUrl($id))
							<img src="{{ LeagueController::getAvatarUrl($id) }}" width="60" class="pull-left" style="margin-right:8px" />
						@endif
					@endif
					{{ Form::label('cover-photo', 'Cover Photo') }}
					{{ Form::file('cover-photo') }}
				</div>

				@if (!isset($id))
					<div class="form-group">
						{{ Form::label('runs-from', 'Runs From') }}
						{{ Form::date('runs-from', '', array('class' => 'form-control', 'placeholder' => 'Runs From')) }}
					</div>

					<div class="form-group">
						{{ Form::label('runs-to', 'Runs To') }}
						{{ Form::date('runs-to', '', array('class' => 'form-control', 'placeholder' => 'Runs To')) }}
					</div>

					<div class="form-group">
						{{ Form::label('wager', 'Wager') }}
						{{ Form::number('wager', '', array('class' => 'form-control', 'placeholder' => 'Wager')) }}
					</div>

					<div class="form-group">
						{{ Form::label('max-teams', 'Number of Teams') }}
						{{ Form::number('max-teams', '', array('class' => 'form-control', 'placeholder' => 'Number of Teams')) }}
					</div>

					<div class="form-group">
						{{ Form::label('private', 'Make this league private (invite only)') }}
						{{ Form::checkbox('private', '1', false) }}
					</div>
				@endif

				@if (isset($id))
					<input type="hidden" name="id" value="{{$id}}" />
					{{ Form::submit('Save', array('class' => 'btn btn-default')) }}
				@else
					{{ Form::submit('Create', array('class' => 'btn btn-default')) }}
				@endif

			{{ Form::close() }}

		</div>

	</div>

@endsection