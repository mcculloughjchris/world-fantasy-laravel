@extends('layouts.master')

@section('title', 'Fantasy Articles - My Teams')

@section('content')

	<div>
		<div class="panel panel-default">
			<div class="panel-heading">Create Team</div>
			<div class="panel-body">
				{{Form::open(array('url' => '/my-teams/create', 'method' => 'post'))}}

					<div class="form-group">
						{{ Form::label('name', 'Name') }}
						{{ Form::text('name', '', ['class' => 'form-control']) }}
					</div>

					<tagselector api="/api/people-list" input_name="selected-members" label="Members" max="8"></tagselector>

					{{ Form::submit('Submit', array('class' => 'btn btn-default')) }}

				{{Form::close()}}
			</div>
		</div>
	</div>

@endsection