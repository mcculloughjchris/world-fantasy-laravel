@extends('layouts.master')

@section('title', 'Fantasy Articles - Buy Credits')

@section('content')

	<div class="panel panel-default">

		<div class="panel-body">
			

			{{Form::open(array('url' => '/buy-credits/', 'method' => 'post'))}}
				<div class="form-group">
					{{Form::label('credits', 'Choose Credits')}}
					{{Form::number('credits', '', array('class' => 'form-control'))}}
				</div>

				{{Form::submit('Pay With Card', array('class' => 'btn btn-default'))}}
			{{Form::close()}}

		</div>

	</div>

@endsection