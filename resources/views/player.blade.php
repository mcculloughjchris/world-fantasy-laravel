@php
$pointTypes = PointsController::getPointTypes();
@endphp

@extends('layouts.master')

@section('title', 'Fantasy Articles')

@section('content')


	<div class="panel panel-default">
		<div class="panel-heading">
			{{$person->name}}
			<div class="pull-right">
				<span data-toggle="tooltip" title="Suggest Name Correction">
					<a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-duplicate"></span></a>
				</span>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        		<h4 class="modal-title" id="myModalLabel">Suggest Name Correction</h4>
							</div>
							<div class="modal-body">
								{{ Form::open(array('url' => '/api/name-suggestion', 'method' => 'post')) }}
									
								{{ Form::close() }}
							</div>
					    </div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body">
			@if (count($stories))
				<table class="table">
					<thead>
						<tr>
							<th>Story</th>
							<th>URL</th>
							@foreach ($pointTypes as $pointType)
								<th>{{$pointType->name}}</th>
							@endforeach
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($stories as $story)
							@php
							$pointsData = PersonController::getPointsData($person->id, $story->id);
							@endphp
							<tr>
								<td>{{ $story->title }}</td>
								<td><a href="{{ $story->url }}" target="_blank">{{ $story->url }} <span class="glyphicon glyphicon-new-window"></span></a></td>
								@php
									$average = 0;
									foreach ($pointTypes as $pointType) {
										$totalPoints = 0;
										foreach ($pointsData['points'][$pointType->name] as $points) {
											$totalPoints += $points->direction;
										}
										echo '<td>' . $totalPoints . '</td>';
										$average += $totalPoints;
									}
									echo '<td>' . $average / count($pointTypes) . '</td>';
								@endphp
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<p><i>This player has no stories</i></p>
			@endif
		</div>
	</div>


@endsection