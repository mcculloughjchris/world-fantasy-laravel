@php

$user = Auth::user();

$username = (isset($name)) ? $name : $user->name;

if (isset($name) && isset($id)) {
	$uid = $id;
} else {
	$uid = $user->id;
}


$joinedLeagues = ProfileController::getJoinedLeagues($uid);
$teams = TeamsController::getUserTeams($uid);
$userData = ProfileController::getUserData($uid);

@endphp

@extends('layouts.master')

@section('title', 'Fantasy Articles - ' . $user->name)

@section('content')

	@php
	$coverPhotoUrl = ProfileController::coverPhotoUrl($uid);
	$coverPhotoClass = ($coverPhotoUrl) ? "move-up" : "";
	@endphp

	@if ($coverPhotoUrl)
		<div class="cover-photo">
			<img src="{{ $coverPhotoUrl }}" />
		</div>
	@endif

	<div class="panel panel-default {{$coverPhotoClass}}">
		<div class="panel-heading profile-heading">
			@if (ProfileController::avatarUrl($uid) !== false)
				<img src="{{ ProfileController::avatarUrl($uid) }}" class="profile-picture" />
			@endif
			<div class="profile-info">
				<span class="profile-username">{{$username}}</span><br>
				<span class="profile-date">Joined on {{ DateController::str(strtotime($userData->created_at)) }}</span>
			</div>
			@if ($uid !== $user->id)
				<div class="panel-heading-actions pull-right">
					<friend-button type="add" username="{{$username}}" uid="{{$uid}}"></friend-button>
				</div>
			@endif
			<div class="clear"></div>
		</div>
	</div>
	@if (!empty($joinedLeagues) && count($joinedLeagues) !== 0)
		<div class="panel panel-default">
			<div class="panel-heading">Joined Leagues</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>League</th>
								<th>Team</th>
								<th>Place</th>
								<th>Joined League on</th>
							</tr>
						</thead>
						<tbody>
							@for ($i = 0; $i < count($joinedLeagues); $i += 1)
								<tr>
									<td><a href="/leagues/{{str_slug($joinedLeagues[$i]->league_name)}}">{{$joinedLeagues[$i]->league_name}}</a></td>
									<td>{{$joinedLeagues[$i]->team_name}}</td>
									<td>{{ TeamsController::getPlace($joinedLeagues[$i]->team_id, $joinedLeagues[$i]->league_id) }}</td>
									<td>{{ DateController::str($joinedLeagues[$i]->joined_on) }}</td>
								</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endif

	@if (!empty($teams))
		<div class="panel panel-default">
			<div class="panel-heading">Teams</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Team</th>
								<th>Players</th>
								<th>Leagues</th>
								<th>Created</th>
							</tr>
						</thead>
						<tbody>

							@foreach ($teams as $k => $v)
								<tr>
									<td>{{$v['team']->name}}</td>
									<td>
										<div class="tags">
											@php
											foreach ($v['players'] as $p) {
												echo PersonController::tag($p->person_id);
											}
											@endphp
										</div>
									</td>
									<td>
										<div class="tags">
											@php
											foreach ($v['leagues'] as $l) {
												echo LeagueController::tag($l->id);
											}
											@endphp
										</div>
									</td>
									<td>{{ DateController::str($v['team']->datestamp) }}</td>
								</tr>
							@endforeach

						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endif

	@php
	ProfileController::areFriends(Auth::user()->id, $uid);
	@endphp

	@if (Auth::user()->id && $uid !== Auth::user()->id)
		@if (ProfileController::areFriends(Auth::user()->id, $uid))
			{{ Form::open(array('url' => '/comment/save', 'method' => 'post')) }}
				<div class="panel panel-default">
					<div class="panel-heading">Comments</div>
					<div class="panel-body">
						<div class="form-group">
							{{ Form::textarea() }}
						</div>
					</div>
				</div>
			{{ Form::close() }}
		@endif
	@endif

@endsection
