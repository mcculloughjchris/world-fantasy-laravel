@extends('layouts.master')

@section('title', 'Fantasy Articles - Join ' . $league->name)

@php

$options = [];

foreach ($myTeams as $team) {
	$options[$team['team']->id] = $team['team']->name;
}

@endphp

@section('content')

		<div class="panel panel-default">

			<div class="panel-heading">Join {{$league->name}}</div>

			<div class="panel-body">

				@foreach ($errors->all() as $error)
					<div class="alert alert-danger">{{$error}}</div>
				@endforeach

				<div class="alert alert-warning"><span class="glyphicon glyphicon-exclamation-sign"></span> Once you join a league you cannot leave!</div>
				
				{{ Form::open(array('url' => '/leagues/join', 'method' => 'post'))}}

					<input type="hidden" name="league-id" value="{{$league->id}}" />

					<div class="form-group">
						{{ Form::label('team-id', 'Team') }}
						{{ Form::select('team-id', $options, null, ['class' => 'form-control', 'placeholder' => 'Select team']) }}
					</div>

					<div class="form-group">
						{{ Form::checkbox('wager', 1) }}
						{{ Form::label('wager', 'Check off this box to accept wagering ' . $league->wager . ' credits.')}}
					</div>

					{{ Form::submit('Join League', ['class' => 'btn btn-default']) }}

				{{ Form::close() }}

			</div>
		</div>

@endsection