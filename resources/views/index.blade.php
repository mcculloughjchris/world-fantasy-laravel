@extends('layouts.master')

@section('title', 'Fantasy Articles')

@section('content')

	@if (session()->has('success'))
		<div class="alert alert-success">
			{{ session()->get('success') }}
		</div>
	@endif

	@if (session()->has('error'))
		<div class="alert alert-error">
			{{ session()->get('error') }}
		</div>
	@endif

	<recent-stories-to-players
		storiesjson="{{ json_encode(App\Http\Controllers\StoryController::getAll(array('limit' => 3))) }}"
		pointtypes="{{ json_encode(App\Http\Controllers\PointsController::getPointTypes()) }}">
	</recent-stories-to-players>

	<top-leagues-top-teams-top-players
		leagues="{{ json_encode(App\Http\Controllers\LeagueController::topLeagues()) }}">
	</top-leagues-top-teams-top-players>

@endsection