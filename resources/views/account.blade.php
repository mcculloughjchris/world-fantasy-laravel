@php

$time_display = Auth::user()->time_display();

@endphp

@extends('layouts.master')

@section('title', 'Fantasy Articles - Account')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">Account Settings</div>
		<div class="panel-body">
			@if (session('success'))
				<div class="alert alert-success">{{session('success')}}</div>
			@endif

			@if (isset($errors))
				@foreach ($errors->all() as $error)
					<div class="alert alert-danger">{{$error}}</div>
				@endforeach
			@endif

			{{Form::open(array('url' => '/account', 'method' => 'post', 'files' => true))}}
				<div class="alert alert-warning">Change your profile pic</div>
				<div class="form-group">
					@if (Auth::user()->avatarUrl() !== false)
						<img src="{{ Auth::user()->avatarUrl() }}" style="width:60px; float: left; margin-right: 10px;" />
					@endif
					{{Form::label('avatar', 'Avatar')}}
					{{Form::file('avatar', '')}}
				</div>
				<div class="alert alert-warning">Change your cover photo</div>
				<div class="form-group">
					@if (Auth::user()->coverPhotoUrl() !== false)
						<img src="{{ Auth::user()->coverPhotoUrl() }}" style="width:60px; float: left; margin-right: 10px;" />
					@endif
					{{ Form::label('cover-photo', 'Cover Photo') }}
					{{ Form::file('cover-photo', '') }}
				</div>
				<div class="alert alert-warning">Change your time settings</div>
				<div class="form-group">
					{{Form::label('time-display', 'Time Display')}}
					{{Form::select('time-display', [
						'mm-dd-yyyy' => 'mm-dd-yyyy',
						'mm/dd/yyyy' => 'mm/dd/yyyy',
						'dd-mm-yyyy' => 'dd-mm-yyyy',
						'dd/mm/yyyy' => 'dd/mm/yyyy'
					], $time_display, ['class' => 'form-control'])}}
				</div>
				<div class="alert alert-warning">Change your email address</div>
				<div class="form-group">
					{{Form::label('email', 'Email')}}
					{{Form::email('email', '', array('placeholder' => 'Email', 'class' => 'form-control'))}}
				</div>
				<div class="alert alert-warning">Change your password</div>
				<div class="form-group">
					{{Form::label('current-password', 'Current password')}}
					{{Form::password('current-password', array('class' => 'form-control', 'placeholder' => 'Current password'))}}
				</div>
				<div class="form-group">
					{{Form::label('password1', 'New password')}}
					{{Form::password('password1', array('class' => 'form-control', 'placeholder' => 'New password'))}}
				</div>
				<div class="form-group">
					{{Form::label('password2', 'Verify new password')}}
					{{Form::password('password2', array('class' => 'form-control', 'placeholder' => 'Verify new password'))}}
				</div>
				{{Form::submit('Save', array('class' => 'btn btn-default'))}}
			{{Form::close()}}
		</div>
	</div>

@endsection