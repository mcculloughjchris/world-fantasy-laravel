<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <link rel="stylesheet" type="text/css" href="/css/app.css" />
    </head>
    <body>

        <div id="vue">
            @section('navigation')
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="/" class="navbar-brand">Fantasy Articles</a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <ul class="nav navbar-nav" style="float:none">
                                <li
                                @if (Request::path() === '/')
                                    class='active'
                                @endif
                                ><a href="/">Home</a></li>

                                    @if (Auth::check() === false)
                                        <li
                                        @if (Request::path() === 'register')
                                            class='active'
                                        @endif
                                        ><a href="/register/">Register</a></li>
                                        <li
                                        @if (Request::path() === 'login')
                                            class='active'
                                        @endif
                                        ><a href="/login/">Login</a></li>

                                    @else

                                        <li
                                        @if (explode('/', Request::path())[0] === 'stories')
                                            class='active'
                                        @endif
                                        ><a href="/stories/">Stories</a></li>
                                        <li
                                        @if (explode('/', Request::path())[0] === 'leagues')
                                            class='active'
                                        @endif
                                        ><a href="/leagues/">Leagues</a></li>
                                        <li class="dropdown pull-right">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li
                                                @if (Request::path() === 'account')
                                                    class='active'
                                                @endif
                                                ><a href="/account/">Account</a></li>
                                                <li
                                                @if (Request::path() === 'profile')
                                                    class='active'
                                                @endif
                                                ><a href="/profile/">Profile</a></li>
                                                <li
                                                @if (Request::path() === 'my-leagues')
                                                    class='active'
                                                @endif
                                                ><a href="/my-leagues/">My Leagues</a></li>
                                                <li
                                                @if (Request::path() === 'my-teams')
                                                    class='active'
                                                @endif><a href="/my-teams/">My Teams</a></li>
                                                <li
                                                @if (Request::path() === 'my-friends')
                                                    class='active'
                                                @endif
                                                ><a href="/my-friends/">My Friends</a></li>
                                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </ul>
                                        </li>
                                        <li class="pull-right">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-bell"></span> <span class="caret"></span></a>
                                            <notificationscomponent></notificationscomponent>
                                        </li>
                                        <li class="pull-right"><a href="/buy-credits"><span class="glyphicon glyphicon-piggy-bank"></span>&nbsp;&nbsp;{{Auth::user()->getCredits()}}</a></li>

                                    @endif


                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
            @show

            <div class="container">
                @section('content');
                @show
            </div>
        </div>

        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
