@php

$user = Auth::user();
$leagues = $user->createdLeagues();

@endphp

@extends('layouts.master')

@section('title', 'Fantasy Articles')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">{{$user->name}}</div>
		<div class="panel-body">
			@if (!empty($leagues))
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Starts</th>
								<th>Ends</th>
								<th># of Teams</th>
								<th>Wagering</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@for ($i = 0; $i < count($leagues); $i += 1)
								<tr>
									<td><a href="{{str_slug($leagues[$i]->name)}}">{{$leagues[$i]->name}}</a></td>
									<td>{{DateController::str($leagues[$i]->runs_from)}}</td>
									<td>{{DateController::str($leagues[$i]->runs_to)}}</td>
									<td>{{count($leagues[$i]->data['teams'])}}/{{$leagues[$i]->max_teams}}</td>
								</tr>
							@endfor
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>

@endsection