<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use DB;
use LeagueController;
use ProfileController;
use stdClass;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getCredits()
    {
        $uid = Auth::user()->id;
        $credits = DB::table('fw_credits')->select('credits')->where('uid', $uid)->get();
        if (count($credits) === 0) {
            DB::table('fw_credits')->insert(
                ['uid' => $uid, 'credits' => '0', 'datestamp' => time()]
            );
            return 0;
        }
        return $credits[0]->credits;
    }

    public static function getJoinedLeagues() {
        if (!Auth::user()) {
            return false;
        }

        $id = Auth::user()->id;

        return ProfileController::getJoinedLeagues($id);
    }

    public static function getTeams()
    {
        if (!Auth::user()) {
            return false;
        }

        $id = Auth::user()->id;
        
        return ProfileController::getTeams($id);
    }

    public static function time_display() {
        if (!Auth::user()) {
            return false;
        }

        $id = Auth::user()->id;

        $time_display = DB::table('users')
            ->where('id', $id)
            ->select('time_display')
            ->get();

        if (count($time_display) === 0) {
            return 'mm/dd/yyyy';
        }

        return $time_display[0]->time_display;

    }

    public static function notificationCount() {
        if (!Auth::user()) {
            return false;
        }

        $id = Auth::user()->id;

        $count = DB::table('fw_notifications')
            ->where([
                ['to_id', '=', $id],
                ['seen', '=', 0]
            ])
            ->count();

        return $count;
    }

    public static function createdLeagues() {
        if (!Auth::user()) {
            return false;
        }

        $id = Auth::user()->id;

        return ProfileController::getCreatedLeagues($id);
    }

    public static function friendship() {
        $returnObj = new stdClass;

        $returnObj->requests = function () {
            $requests = [];
            $query = DB::table('fw_friends')
                ->where([
                    ['user_b', '=', Auth::user()->id],
                    ['approved', '=', 0]
                ])
                ->get();
            for ($i = 0; $i < count($query); $i += 1) {
                $requests[] = $query[$i]->user_a;
            }
            return $requests;
        };

        $returnObj->sent = function () {
            $sent = [];

            $query = DB::table('fw_friends')
                ->where([
                    ['user_a', '=', Auth::user()->id],
                    ['approved', '=', 0]
                ])
                ->get();
            for ($i = 0; $i < count($query); $i += 1) {
                $sent[] = $query[$i]->user_b;
            }
            return $sent;
        };

        $returnObj->friends = function () {
            $friends = [];
            $query = DB::table('fw_friends')
                ->where([
                    ['user_a', '=', Auth::user()->id],
                    ['approved', '=', 1]
                ])
                ->orWhere([
                    ['user_b', '=', Auth::user()->id],
                    ['approved', '=', 1]
                ])
                ->get();
            for ($i = 0; $i < count($query); $i += 1) {
                if ($query[$i]->user_a === Auth::user()->id) {
                    $friends[] = $query[$i]->user_b;
                } else {
                    $friends[] = $query[$i]->user_a;
                }
            }
            return $friends;
        };


        return $returnObj;

    }

    public static function avatarUrl()
    {
        if (!Auth::user()) {
            return false;
        }

        return ProfileController::avatarUrl(Auth::user()->id);
    }

    public static function coverPhotoUrl()
    {
        if (!Auth::user()) {
            return false;
        }

        return ProfileController::coverPhotoUrl(Auth::user()->id);
    }
}
