<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PersonController extends Controller
{

    public static function get($id = null)
    {
        if ($id === null) {
            return false;
        }

        $person = DB::table('fw_people')->where('id', $id)->get();
        return $person[0];
    }

    public static function getPointsData($id = null, $story_id = null)
    {
        if ($id === null) {
            return false;
        }

        $point_types = PointsController::getPointTypes();
        $point_type_count = count($point_types);

        $data = array();

        if ($story_id === null) {
            for ($i = 0; $i < $point_type_count; $i += 1) {
                $p = DB::table('fw_points')->where([
                    ['person_id', '=', $id],
                    ['point_type_id', '=', $point_types[$i]->id]
                ])->get();
                if (!empty($p)) {
                    $data['points'][$point_types[$i]->name] = $p;
                } else {
                    $data['points'][$point_types[$i]->name] = new stdClass();
                }
            }
        } else {
            for ($i = 0; $i < $point_type_count; $i += 1) {
                $p = DB::table('fw_points')->where([
                    ['news_id', '=', $story_id],
                    ['person_id', '=', $id],
                    ['point_type_id', '=', $point_types[$i]->id]
                ])->get();
                if (!empty($p)) {
                    $data['points'][$point_types[$i]->name] = $p;
                } else {
                    $data['points'][$point_types[$i]->name] = [];
                }
            }
        }

        $data['average'] = 0;

        foreach ($data['points'] as $key => $value) {
            for ($i = 0; $i < count($value); $i += 1) {
                $data['average'] += $value[$i]->direction;
            }
        }

        $data['average'] = $data['average'] / $point_type_count;

        return $data;
    }

    public static function getStories($id = null, $limit = 10)
    {
        if ($id === null) {
            return false;
        }

        $stories = DB::table('fw_news_people')
            ->join('fw_news', 'fw_news_people.news_id', '=', 'fw_news.id')
            ->where('person_id', $id)
            ->limit($limit)
            ->get();

        return $stories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index($returns = 'sql')
    {
        $people = DB::table('fw_people')->get();

        if ($returns === 'sql') {
            for ($i = 0; $i < count($people); $i += 1) {
                $people[$i]->value = $people[$i]->name;
            }
            return $people;
        } else if ($returns === 'array') {
            $list = [];
            $list[] = '';
            for ($i = 0; $i < count($people); $i += 1) {
                $list[$people[$i]->id] = $people[$i]->name;
            }
            return $list;
        }
    }

    public static function tag($id = null)
    {
        if ($id === null) {
            return false;
        }

        $name = DB::table('fw_people')
            ->where('id', $id)
            ->get();

        if (empty($name)) {
            return false;
        }

        return "<span class='tag'>{$name[0]->name} <a href='/players/{$name[0]->id}'><span class='glyphicon glyphicon-eye-open'></span></a></span>";
    }

    public static function generateImages()
    {
        return false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
