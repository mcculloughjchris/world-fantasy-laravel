<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TeamsController extends Controller
{

    public static function generateTeamData($id)
    {
        $data = array();

        $data['team'] = DB::table('fw_teams')
            ->where('id', $id)
            ->get()[0];
        $data['players'] = DB::table('fw_team_players')
            ->where('team_id', $data['team']->id)
            ->join('fw_people', 'fw_team_players.person_id', '=', 'fw_people.id')
            ->get();
        $data['leagues'] = DB::table('fw_leagues_teams')
            ->join('fw_leagues', 'fw_leagues_teams.league_id', '=', 'fw_leagues.id')
            ->where('team_id', $data['team']->id)
            ->get();

        return $data;
    }

    public static function getUserTeams($uid = null)
    {
        if ($uid === null) {
            return false;
        }

        $array = [];

        $teams = DB::table('fw_teams')
            ->select('id')
            ->where('user_id', $uid)
            ->get();

        for ($i = 0; $i < count($teams); $i += 1) {
            $array[$teams[$i]->id] = TeamsController::generateTeamData($teams[$i]->id);
        }


        return $array;
    }

    public static function getLeagueTeams($lid = null)
    {
        if ($lid === null) {
            return false;
        }

        $array = [];

        $teams = DB::table('fw_leagues_teams')
            ->select('team_id')
            ->where('league_id', $lid)
            ->get();

        if (empty($teams)) {
            return [];
        }

        foreach ($teams as $team) {
            $array[$team->team_id] = TeamsController::generateTeamData($team->team_id);
        }

        return $array;
    }

    public static function getScore($teamId = null, $leagueId = null)
    {
        if ($teamId === null ||
                $leagueId === null) {
            return false;
        }

        $league = LeagueController::get($leagueId);

        if ($league === false) {
            return false;
        }

        $team = TeamsController::generateTeamData($teamId);

        $pointTypeCount = count(PointsController::getPointTypes());

        $scores = [];

        for ($i = 0; $i < count($team['players']); $i += 1) {
            $score = 0;
            $scoreData = DB::table('fw_points')
                ->where([
                    ['datestamp', '>=', $league->runs_from],
                    ['datestamp', '<=', $league->runs_to],
                    ['person_id', '=', $team['players'][$i]->person_id]
                ])
                ->get();

            for ($x = 0; $x < count($scoreData); $x += 1) {
                $score += $scoreData[$i]->direction;
            }

            $score = $score / $pointTypeCount;
            $scores[] = $score;
        }

        $finalScore = 0;

        for ($i = 0; $i < count($scores); $i += 1) {
            $finalScore += $scores[$i];
        }

        return $finalScore / count($team['players']);

    }

    public static function getPlace($team_id = null, $league_id = null)
    {
        if ($league_id === null ||
                $team_id === null) {
            return false;
        }

        $teams = DB::table('fw_leagues_teams')
            ->where('league_id', $league_id)
            ->get();

        $scores = [];

        for ($i = 0; $i < count($teams); $i += 1) {
            $scores[$teams[$i]->team_id] = TeamsController::getScore($teams[$i]->team_id, $league_id);
        }

        arsort($scores);

        $place = 0;
        $ext = '';
        foreach ($scores as $t => $s) {
            $place += 1;
            if ($t === $team_id) {
                if ($place === 1) {
                    $ext = 'st';
                } else if ($place === 2) {
                    $ext = 'nd';
                } else if ($place === 3) {
                    $ext = 'rd';
                } else if ($place > 3) {
                    $ext = 'th';
                }

                return "{$place}{$ext}";
            }
        }

        return false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
