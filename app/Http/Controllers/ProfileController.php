<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class ProfileController extends Controller
{
    public static function getUserData($id = null)
    {
        if ($id === null) {
            return false;
        }

        $user = new stdClass();
        $user->id = $id;

        $data = DB::table('users')
            ->where('id', $id)
            ->select('name', 'created_at')
            ->get()[0];

        if ($data->name !== null &&
                isset($data->name)) {
            $user->name = $data->name;
        }

        if ($data->created_at !== null &&
                isset($data->created_at)) {
            $user->created_at = $data->created_at;
        }

        return $user;
    }

    public static function getJoinedLeagues($id = null)
    {

    	if ($id === null) {
    		return false;
    	}

        $data = DB::table('fw_teams')
            ->join('fw_leagues_teams', 'fw_teams.id', '=', 'fw_leagues_teams.team_id')
            ->join('fw_leagues', 'fw_leagues_teams.league_id', '=', 'fw_leagues.id')
            ->where('fw_teams.user_id', $id)
            ->select('fw_teams.name as team_name', 'fw_teams.id as team_id', 'fw_leagues.name as league_name', 'fw_leagues.id as league_id', 'fw_leagues_teams.datestamp as joined_on')
            ->get();

        return $data;
    }

    public static function getTeams($id = null)
    {
    	if ($id === null) {
    		return false;
    	}

        $teams = DB::table('fw_teams')
            ->where('user_id', $id)
            ->get();

        return $teams;
    }

    public static function getCreatedLeagues($id = null)
    {
    	if ($id === null) {
    		return false;
    	}

    	$leagues = [];
        $sql = DB::table('fw_leagues')
            ->where('user_id', $id)
            ->get();

        for ($i = 0; $i < count($sql); $i += 1) {
            $sql[$i]->data = LeagueController::getLeagueData($sql[$i]->id);
        }

        return $sql;
    }

    public static function friends($id = null)
    {
        if ($id === null) {
            return false;
        }

        $a = DB::table('fw_friends')
            ->where([
                ['user_a', '=', $id],
                ['approved', '=', 1]
            ])
            ->get();

        $b = DB::table('fw_friends')
            ->where([
                ['user_b', '=', $id],
                ['approved', '=', 1]
            ])
            ->get();

        if (count($a) === 0 && count($b) === 0) {
            return [];
        }

        $array = [];

        for ($i = 0; $i < count($a); $i += 1) {
            if ($a[$i]->user_a === $id) {
                $user = ProfileController::getUserData($a[$i]->user_b);
            } else {
                $user = ProfileController::getUserData($a[$i]->user_a);
            }
            $user->value = $user->name;
            $array[] = $user;
        }

        for ($i = 0; $i < count($b); $i += 1) {
          if ($b[$i]->user_a === $id) {
              $user = ProfileController::getUserData($b[$i]->user_b);
          } else {
              $user = ProfileController::getUserData($b[$i]->user_a);
          }
          $user->value = $user->name;
          $array[] = $user;
        }

        return $array;
    }

    public static function areFriends($a = null, $b = null)
    {
      if ($a === null || $b === null) {
        return false;
      }

      $bool = false;

      $aFriends = ProfileController::friends($a);

      print($bool);

      for ($i = 0; $i < count($aFriends); $i += 1) {
        if ($aFriends[$i]->id === $b) {
          $bool = true;
        }
      }
      return $bool;
    }

    public static function avatarUrl($id = null)
    {
        if ($id === null) {
            return false;
        }

        if (Storage::exists('images/avatars/' . $id . '.jpeg')) {
            return str_replace('storage/', '', Storage::url('images/avatars/' . $id . '.jpeg'));
        }

        if (Storage::exists('images/avatars/' . $id . '.jpg')) {
            return str_replace('storage/', '', Storage::url('images/avatars/' . $id . '.jpg'));
        }

        return false;
    }

    public static function coverPhotoUrl($id = null)
    {
        if ($id === null) {
            return false;
        }

        if (Storage::exists('images/avatars/' . $id . '-cover-photo.jpeg')) {
            return str_replace('storage/', '', Storage::url('images/avatars/' . $id . '-cover-photo.jpeg'));
        }

        if (Storage::exists('images/avatars/' . $id . '-cover-photo.jpg')) {
            return str_replace('storage/', '', Storage::url('images/avatars/' . $id . '-cover-photo.jpg'));
        }

        return false;
    }
}
