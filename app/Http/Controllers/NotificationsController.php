<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use DB;
use TeamsController;
use Auth;

class NotificationsController extends Controller
{

	public static function generateLeagueNotifications() {
		/**
		 * Select all the leagues that ended and aren't notified
		 */
		$leagues = DB::table('fw_leagues')
			->where([
				['runs_to', '<=', time()],
				['notified', '=', '0']
			])
			->get();

		/**
		 * Loop through the leagues
		 */
		for ($i = 0; $i < count($leagues); $i += 1) {
			/**
			 * Get the teams in this league
			 */
			$teams = DB::table('fw_leagues_teams')
				->where('league_id', $leagues[$i]->id)
				->get();
			
			/**
			 * Get points for each team and re-sort by most points
			 */
			$pointsArray = [];
			
			for ($x = 0; $x < count($teams); $x += 1) {
				$pointsArray[$teams[$x]->id] = TeamsController::getScore($teams[$x]->team_id, $leagues[$i]->id);
			}
			
			arsort($pointsArray);
			reset($pointsArray);

			$winningteam = key($pointsArray);

			for ($x = 0; $x < count($teams); $x += 1) {
				$user_id = DB::table('fw_teams')
					->select('user_id')
					->where('id', $teams[$x]->team_id)
					->get()[0]->user_id;

                NotificationsController::saveNotification('league_ended', $user_id, $leagues[$i]->id);
			}

			DB::table('fw_leagues')
				->where('id', $leagues[$i]->id)
				->update(['notified' => '1']);

		}
	}

    public static function generateAllNotifications() {
    	NotificationsController::generateLeagueNotifications();
    }

    public static function parseNotification($id) {
    	if ($id === null) {
    		return false;
    	}

    	$notificationData = DB::table('fw_notifications')
    		->where('id', $id)
    		->get()[0];

    	$notificationTypeData = DB::table('fw_notification_types')
    		->where('id', $notificationData->type_id)
    		->get()[0];

    	switch ($notificationTypeData->type) {
    		case 'league_ended':
    			$to = DB::table('users')
    				->select('name')
    				->where('id', $notificationData->to_id)
    				->get()[0]->name;
    			$from = DB::table('fw_leagues')
    				->select('name')
    				->where('id', $notificationData->from_id)
    				->get()[0]->name;

                if ($to === Auth::user()->name) {
                    $to = 'You';
                }

    			$search = array(
    				'{{to}}',
    				'{{from}}'
    			);
    			$replace = array(
    				$to,
    				$from
    			);

    			$msg = str_replace($search, $replace, $notificationTypeData->msg);
    		break;
            case 'frnd_request':
            case 'frnd_denied':
            case 'frnd_accept':
            case 'frnd_deletd':
                $from = ProfileController::getUserData($notificationData->from_id);

                $msg = str_replace('{{from}}', $from->name, $notificationTypeData->msg);
            break;
            case 'lg_invite':
                $from = LeagueController::get($notificationData->from_id);

                $text = "<a href='/league/" . str_slug($from->name) . "' class='normal-link'>{$from->name}</a>";

                $msg = str_replace('{{from}}', $text, $notificationTypeData->msg);
            break;
            case 'frnd_lg':
                $from = ProfileController::get($notificationData->from_id);
                $league = LeagueController::get($notificationData->alt_id);

                $search = array(
                    '{{from}}',
                    '{{url}}'
                );

                $replace = array(
                    $from->name,
                    '/leagues/' . str_slug($league->name)
                );

                $msg = str_replace($search, $replace, $notificationTypeData->msg);
            break;
    	}

    	if (isset($msg)) {
    		return $msg;
    	}
    }

    public static function saveNotification($type = null, $toId = null, $fromId = null, $altId = 0)
    {
        if ($type === null ||
                $toId === null ||
                $fromId === null) {
            return false;
        }

        $notification_type_id = DB::table('fw_notification_types')
            ->select('id')
            ->where('type', $type)
            ->get();

        if (count($notification_type_id) === 0) {
            return false;
        }

        $notification_type_id = $notification_type_id[0];

        if (!isset($notification_type_id->id)) {
            return false;
        }

        $notification_type_id = $notification_type_id->id;

        $insert = DB::table('fw_notifications')
            ->insert([
                'to_id' => $toId,
                'from_id' => $fromId,
                'type_id' => $notification_type_id,
                'alt_id' => $altId,
                'datestamp' => time()
            ]);

        return true;
    }

    public static function displayNotifications() {
    	if (!Auth::user()) {
    		return false;
    	}

    	$uid = Auth::user()->id;

    	$notifications = DB::table('fw_notifications')
    		->where('to_id', $uid)
    		->orderBy('id', 'desc')
    		->limit('5')
    		->get();

    	$html = '';

    	for ($i = 0; $i < count($notifications); $i += 1) {
    		$html .= '<li';

    		if ($notifications[$i]->seen === 0) {
    			$html .= ' class="new"';
    		}

    		$html .= ' data-id="' . $notifications[$i]->id . '">' . NotificationsController::parseNotification($notifications[$i]->id) . ' <span class="glyphicon glyphicon-remove"></span></li>';
    	}

    	return $html;

    }
}
