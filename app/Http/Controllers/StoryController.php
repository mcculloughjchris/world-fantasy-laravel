<?php

namespace App\Http\Controllers;

use Request;
use DB;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {
        return view('stories', array(
            'stories' => StoryController::getAll(),
            'pagination' => StoryController::pagination()
        ));
    }

    public static function getAll($args = array())
    {
        $stories = DB::table('fw_news');

        $page = Request::get('page');
            $page = ($page !== '') ? $page : 1;

        if (!isset($args['limit'])) {
            $limit = 10;
        } else {
            $limit = $args['limit'];
        }

        $offset = ($page - 1) * $limit;

        $q = Request::get('q');

        if (strlen($q) !== 0) {
            $stories = $stories->where('title', 'LIKE', '%' . $q . '%');
        }

        $sortBy = 'id';
        $orderBy = 'desc';

        if (Request::get('by') && Request::get('sort')) {
            if (Request::get('by') === 'name') {
                $sortBy = 'title';
            }
            if (Request::get('by') === 'date') {
                $sortBy = 'datestamp';
            }
            if (Request::get('by') === 'source') {
                $sortBy = 'url';
            }

            if (Request::get('sort') === 'asc') {
                $orderBy = 'asc';
            } else {
                $orderBy = 'desc';
            }
        }

        $stories = $stories->offset($offset)->limit($limit)->orderBy($sortBy, $orderBy);

        $stories = $stories->get();

        foreach ($stories as $story) {
            $story->date = DateController::str($story->datestamp);
        }

        return $stories;
    }

    public static function pagination() {
        $stories = DB::table('fw_news');

        $limit = Request::get('limit');
            $limit = (strlen($limit) !== 0) ? $limit : 10;

        $q = Request::get('q');

        if (strlen($q) !== 0) {
            $stories = $stories->where('title', 'LIKE', '%' . $q . '%');
        }

        $count = $stories->count();

        return view('pagination', array(
            'url' => 'stories',
            'q' => Request::get('q'),
            'pages' => ceil($count / $limit)
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        $story = DB::table('fw_news')->where('id', $id)->get();
        return view('stories', array(
            'story' => $story[0],
            'people' => StoryController::getPeople($story[0]->id),
            'pointTypes' => PointsController::getPointTypes()
        ));
    }

    public static function getPeople($id = null)
    {
        if ($id === null) {
            return false;
        }

        $array = [];

        $people = DB::table('fw_news_people')->select('person_id')->where('news_id', $id)->get();

        for ($i = 0; $i < count($people); $i += 1) {
            $array[$i] = [];
            $array[$i]['person'] = PersonController::get($people[$i]->person_id);
            $array[$i]['points'] = PersonController::getPointsData($people[$i]->person_id, $id);
            $array[$i]['pointsDataRow'] = PointsController::displayPointsDataRow(
                $array[$i]['person'],
                $array[$i]['points'],
                true,
                $id
            );
        }

        return $array;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
