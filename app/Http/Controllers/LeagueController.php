<?php

namespace App\Http\Controllers;

use Request;
use DB;
use Auth;
use Validator;
use NotificationsController;
use Storage;

class LeagueController extends Controller
{

    /**
     * This gets a bunch of data about the team specified
     */
    public static function getLeagueData($id)
    {
        /**
         * If the user is logged in cache the user's id
         */
        if (Auth::user()) {
            $uid = Auth::user()->id;
        /**
         * The user isn't logged in, set the user's ID to 0
         */
        } else {
            $uid = '0';
        }

        /**
         * This is where we'll keep all the data
         */
        $arr = [];

        /**
         * By default set the userJoined variable to 0
         */
        $arr['userJoined'] = 0;

        /**
         * Get all the teams that joined this league
         */
        $arr['teams'] = DB::table('fw_leagues_teams')
            ->where('league_id', $id)
            ->join('fw_teams', 'fw_leagues_teams.team_id', '=', 'fw_teams.id')
            ->get();

        /**
         * Get all the users that were invited to this league
         */
        $arr['invites'] = DB::table('fw_leagues_invites')
            ->where('league_id', $id)
            ->get();

        /**
         * Loop through all the teams
         */
        for ($i = 0; $i < count($arr['teams']); $i += 1) {
            /**
             * Check if this user joined the league
             */
            if ($arr['teams'][$i]->user_id === $uid) {
                /**
                 * Set userJoined so we can use it later
                 */
                $arr['userJoined'] = 1;
            }
        }

        /**
         * Return the array of data
         */
        return $arr;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {
        return view('leagues', array(
            'leagues' => LeagueController::getAll(time()),
            'pagination' => LeagueController::pagination(),
            'name' => null
        ));
    }

    /**
     * This returns the page to join a league
     */
    public static function join($id)
    {
        $league = LeagueController::get($id);
        return view('join-league', array(
            'id' => $id,
            'league' => $league,
            'myTeams' => TeamsController::getUserTeams(Auth::user()->id)
        ));
    }

    /**
     * This gets all the leagues and checks for search or sort queries
     */
    public static function getAll($from = null, $to = false)
    {
        $leagues = DB::table('fw_leagues');

        $page = Request::get('page');
            $page = ($page !== '') ? $page : 1;

        $limit = 10;
        $offset = ($page - 1) * $limit;

        $q = Request::get('q');

        if (strlen($q) !== 0) {
            $leagues = $leagues->where('name', 'LIKE', '%' . $q . '%');
        }

        if ($from !== null) {
            $leagues = $leagues->where('runs_from', '<=', $from);
        }

        if ($to !== false) {
            $leagues = $leagues->where('runs_to', '>', time());
        }

        $sortBy = 'id';
        $orderBy = 'desc';

        if (Request::get('by') && Request::get('sort')) {
            if (Request::get('by') === 'league') {
                $sortBy = 'name';
            }
            if (Request::get('by') === 'starts') {
                $sortBy = 'runs_from';
            }
            if (Request::get('by') === 'ends') {
                $sortBy = 'runs_to';
            }
            if (Request::get('by') === 'teams') {
                $sortBy = 'max_teams';
            }
            if (Request::get('by') === 'wager') {
                $sortBy = 'wager';
            }

            if (Request::get('sort') === 'asc') {
                $orderBy = 'asc';
            } else {
                $orderBy = 'desc';
            }
        }

        $leagues = $leagues
            ->offset($offset)
            ->limit($limit)
            ->orderBy($sortBy, $orderBy);

        $leagues = $leagues->get();

        for ($i = 0; $i < count($leagues); $i += 1) {
            $data = LeagueController::getLeagueData($leagues[$i]->id);
            $leagues[$i]->teams = DB::table('fw_leagues_teams')
                ->where('league_id', $leagues[$i]->id)
                ->join('fw_teams', 'fw_leagues_teams.team_id', '=', 'fw_teams.id')
                ->get();
            for ($x = 0; $x < count($leagues[$i]->teams); $x += 1) {
                if ($leagues[$i]->teams[$x]->user_id === Auth::user()->id) {
                    $leagues[$i]->userJoined = 1;
                }
            }
        }

        return $leagues;
    }

    public static function pagination()
    {
        $leagues = DB::table('fw_leagues');

        $limit = Request::get('limit');
            $limit = (strlen($limit) !== 0) ? $limit : 10;

        $q = Request::get('q');

        if (strlen($q) !== 0) {
            $leagues = $leagues->where('name', 'LIKE', '%' . $q . '%');
        }

        $count = $leagues->count();

        return view('pagination', array(
            'url' => 'leagues',
            'q' => Request::get('q'),
            'pages' => ceil($count / $limit)
        ));
    }

    public static function getTeams($id = null)
    {
        if ($id === null) {
            return false;
        }
        return TeamsController::getLeagueTeams($id);
    }

    public static function tag($id = null) {
        if ($id === null) {
            return false;
        }
        $name = DB::table('fw_leagues')
            ->select('name')
            ->where('id', $id)
            ->limit(1)
            ->get();
        if (empty($name)) {
            return false;
        }

        if (!isset($name[0]->name)) {
            return false;
        }

        return "<span class='tag'>{$name[0]->name}<a href='/leagues/{$name[0]->name}'><span class='glyphicon glyphicon-eye-open'></span></a></span>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $name 
     * @return \Illuminate\Http\Response
     */
    public static function show($name)
    {
        $name = urldecode(str_replace('-', ' ', $name));
        $league = DB::table('fw_leagues')->whereRaw("LOWER(name) = ?", [$name])->get()[0];

        $data = LeagueController::getLeagueData($league->id);
        $invitedUsers = [];

        foreach ($data['invites'] as $invite) {
            $invitedUsers[] = $invite->to_id;
        }

        if ($league->private === 1) {
            if (!Auth::user()) {
                return redirect('/login');
            }

            if ($league->user_id !== Auth::user()->id &&
                    !in_array(Auth::user()->id, $invitedUsers) &&
                    $data['userJoined'] === 0) {
                return redirect('/');
            }
        }

        return view('leagues', array(
            'league' => LeagueController::get($league->id),
            'pointTypes' => PointsController::getPointTypes()
        ));
    }

    public static function get($id)
    {
        $data = LeagueController::getLeagueData($id);

        $league = DB::table('fw_leagues')
            ->where('id', $id)
            ->get();

        $data = LeagueController::getLeagueData($id);

        $league[0]->teams = LeagueController::getTeams($id);
        $league[0]->userJoined = $data['userJoined'];

        return $league[0];
    }

    /**
     * Creates a new league
     */
    public static function save()
    {
        if (Request::get('id')) {
            return LeagueController::update();
        }

        /**
         * Setup some variables
         */
        $name = Request::get('name');
        $runsFrom = strtotime(Request::get('runs-from'));
        $runsTo = strtotime(Request::get('runs-to'));
        $wager = Request::get('wager');
        $maxTeams = Request::get('max-teams');
        $private = Request::get('private');

        if (isset($private) === false) {
            $private = 0;
        }

        /**
         * Setup validation for these variables
         */
        $validator = Validator::make(
            [
                'Name' => $name,
                'Runs From' => $runsFrom,
                'Runs To' => $runsTo,
                'Wager' => $wager,
                'Number Of Teams' => $maxTeams
            ],
            [
                'Name' => 'required|string',
                'Runs From' => 'required|integer',
                'Runs To' => 'required|integer',
                'Wager' => 'required|integer',
                'Number Of Teams' => 'required|integer',
            ]
        );

        /**
         * If the validation fails, redirect with errors
         */
        if ($validator->fails()) {
            return redirect('/leagues/create')->withInput()->withErrors($validator);
        }

        /**
         * Check if a league with this name already exists
         */
        $check = DB::table('fw_leagues')->where('name', $name)->count();
        $checkResult = ($check === 0) ? true : false;

        /**
         * Setup validation
         */
        $validator = Validator::make(
            [ 'exists' => $checkResult ],
            [ 'exists' => 'accepted' ],
            [ 'exists' => 'There is already a league with this name' ]
        );

        /**
         * If validation fails, redirect with errors
         */
        if ($validator->fails()) {
            return redirect('/leagues/create')->withInput()->withErrors($validator);
        }

        /**
         * We made it this far, save new league
         */
        $id = DB::table('fw_leagues')
            ->insertGetId([
                'user_id' => Auth::user()->id,
                'name' => $name,
                'runs_from' => $runsFrom,
                'runs_to' => $runsTo,
                'wager' => $wager,
                'today' => strtotime('today'),
                'max_teams' => $maxTeams,
                'private' => $private,
                'datestamp' => time()
            ]);

        if ($id !== null) {
            LeagueController::saveImages($id);
        }

        $friends = ProfileController::friends(Auth::user()->id);

        for ($i = 0; $i < count($friends); $i += 1) {
            NotificationsController::saveNotification('frnd_lg', $friends[$i], Auth::user()->id, $id);
        }

        /**
         * Redirect to the new league's view page
         */
        return redirect('/leagues/' . $name . '/');
    }

    /**
     * Joins a league
     */
    public static function doJoin()
    {
        /**
         * If there's no user, redirect them to the login page
         */
        if (!Auth::user()) {
            return redirect('/login');
        }

        /**
         * Select how many credits the user already has
         */
        $credits = Auth::user()->getCredits();

        /**
         * Setup other variables
         */
        $leagueId = intval(Request::get('league-id'));

        /**
         * Get league info
         */
        $league = DB::table('fw_leagues')
            ->where('id', $leagueId)
            ->get();
        if (count($league) === 0) {
            return false;
        }
        $league = $league[0];

        /**
         * Setup more variables
         */
        $teamId = Request::get('team-id');
        $wager = intval(Request::get('wager'));
        $afterWager = $credits - $league->wager;

        /**
         * Check if this is private
         */
        $private = $league->private;

        /**
         * Select the maximum amount of teams for this league
         */
        $maxTeams = $league->max_teams;

        /**
         * Select how many teams already joined this league
         */
        $numOfTeams = DB::table('fw_leagues_teams')
            ->where('league_id', $leagueId)
            ->count();

        /**
         * Setup validator
         */
        $validationInput = [
            'wager' => $wager,
            'Team' => $teamId,
            'League' => $leagueId,
            'after-wager' => $afterWager,
            'maxed out' => $numOfTeams + 1
        ];

        $validationRules = [
            'wager' => 'required|accepted',
            'Team' => 'required|integer',
            'League' => 'required|integer',
            'after-wager' => 'integer|min:0',
            'maxed out' => 'max:' . $maxTeams
        ];

        $validationMsgs = [
            'League.required' => 'Something went wrong joining this league.',
            'League.integer' => 'Something went wrong joining this league.',
            'after-wager.min' => 'You do not have enough credits',
            'after-wager.integer' => 'You do not have enough credits'
        ];

        /**
         * If the league is private
         */
        if ($private === 1) {
            /**
             * Check if the use is allowed to join
             */
            $check = DB::table('fw_leagues_invites')
                ->where([
                    ['league_id', '=', $leagueId],
                    ['to_id', '=', Auth::user()->id]
                ])
                ->count();

            if ($check === 1) {
                $validationInput['private'] = 1;
                $validationRules['private'] = 'min:1';
                $validationMsgs['private.min'] = 'This is a private league';
            }
        }

        /**
         * Continue setting up the validator
         */
        $validator = Validator::make(
            $validationInput,
            $validationRules,
            $validationMsgs
        );

        /**
         * If validator fails, redirect with errors
         */
        if ($validator->fails()) {
            return redirect('/leagues/join/' . $leagueId)->withInput()->withErrors($validator);
        }

        /**
         * Check if this team already joined this league
         */
        $new = DB::table('fw_leagues_teams')->where([
            ['league_id', '=', $leagueId],
            ['team_id', '=', $teamId]
        ])->count();

        if (intval($new) === 0) {
            $new = true;
        } else {
            $new = false;
        }

        /**
         * Setup validator
         */
        $validator = Validator::make(
            [
                'Exists' => $new
            ],
            [
                'Exists' => 'required|accepted'
            ],
            [
                'Exists' => 'You already joined this league with this team.'
            ]
        );

        /**
         * If the validator failed, redirect with errors
         */
        if ($validator->fails()) {
            return redirect('/leagues/join/' . $leagueId)->withInput()->withErrors($validator);
        }

        /**
         * Select how much this league's wager is
         */
        $realWager = DB::table('fw_leagues')
            ->select('wager')
            ->where('id', $leagueId)
            ->get()[0]->wager;

        /**
         * Remove credits from user
         */
        DB::table('fw_credits')
            ->where('uid', Auth::user()->id)
            ->decrement('credits', $realWager);

        /**
         * Add team to league
         */
        DB::table('fw_leagues_teams')->insert([
            ['league_id' => $leagueId, 'team_id' => $teamId, 'datestamp' => time()]
        ]);

        /**
         * Remove invite
         */
        DB::table('fw_leagues_invites')
            ->where([
                ['league_id', '=', $leagueId],
                ['to_id', '=', Auth::user()->id]
            ])
            ->delete();

        /**
         * Get the league name
         */
        $name = DB::table('fw_leagues')
            ->select('name')
            ->where('id', $leagueId)
            ->get();

        /**
         * Redirect to league's view page
         */
        return redirect('/leagues/' . $name[0]->name);
    }

    /**
     * Invites a user to a laegue
     */
    public static function inviteUser($league_id = null, $from_id = null, $invite = null)
    {
        if ($invite === null ||
                $from_id === null ||
                $league_id === null) {
            return false;
        }

        // check if already invited
        $check = DB::table('fw_leagues_invites')
            ->where([
                ['league_id', '=', $league_id],
                ['to_id', '=', $invite]
            ])
            ->count();

        if ($check !== 0) {
            return false;
        }

        DB::table('fw_leagues_invites')
            ->insert([
                'league_id' => $league_id,
                'from_id' => $from_id,
                'to_id' => $invite,
                'datestamp' => time()
            ]);

        NotificationsController::saveNotification('lg_invite', $invite, $league_id);

        return true;
    }

    public static function doInvite()
    {
        $league_id = Request::get('league_id');
        $invited = json_decode(Request::get('invited'));
        $uid = Auth::user()->id;

        if (isset($league_id) && $league_id !== null) {
            for ($i = 0; $i < count($invited); $i += 1) {
                LeagueController::inviteUser($league_id, $uid, $invited[$i]->id);
            }
        }

        $league = DB::table('fw_leagues')
            ->where('id', $league_id)
            ->select('name')
            ->get();

        if (count($league) !== 0) {
            if (isset($league[0]->name)) {
                return redirect('/leagues/' . str_slug($league[0]->name));
            }
        }

    }

    public static function getAvatarUrl($id = null)
    {
        if ($id === null) {
            return false;
        }

        if (Storage::exists('images/leagues/' . $id . '.jpeg')) {
            return str_replace('storage/', '', Storage::url('images/leagues/' . $id . '.jpeg'));
        }

        if (Storage::exists('images/leagues/' . $id . '.jpg')) {
            return str_replace('storage/', '', Storage::url('images/leagues/' . $id . '.jpg'));
        }

        return false;
    }

    public static function getCoverPhotoUrl($id = null)
    {
        if ($id === null) {
            return false;
        }

        if (Storage::exists('images/leagues/' . $id . '-cover-photo.jpeg')) {
            return str_replace('storage/', '', Storage::url('images/leagues/' . $id . '-cover-photo.jpeg'));
        }

        if (Storage::exists('images/leagues/' . $id . '-cover-photo.jpg')) {
            return str_replace('storage/', '', Storage::url('images/leagues/' . $id . '-cover-photo.jpg'));
        }

        return false;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public static function saveImages($id = null)
    {
        if ($id === null) {
            return false;
        }

        /**
         * If there's a file, store it locally
         */
        if (!empty(Request::allFiles())) {

            if (Request::file('avatar')) {
                $validator = Validator::make(
                    ['Avatar' => Request::file('avatar')],
                    ['Avatar' => 'mimes:jpeg,jpg | max:1000']
                );

                if ($validator->fails()) {
                    return redirect('/leagues/create')->withInput()->withErrors($validator);
                }

                /**
                 * Delete the file in case it exists
                 */
                if (Storage::exists('images/leagues/' . $id . '.jpeg')) {
                    Storage::delete('images/leagues/' . $id . '.jpeg');
                }

                if (Storage::exists('images/leagues/' . $id . '.jpg')) {
                    Storage::delete('images/leagues/' . $id . '.jpg');
                }

                /**
                 * Save the new file
                 */
                Request::file('avatar')->storeAs(
                    'images/leagues', $id . '.' . Request::file('avatar')->extension()
                );
            }

            if (Request::file('cover-photo')) {
                $validator = Validator::make(
                    ['Cover Photo' => Request::file('cover-photo')],
                    ['Cover Photo' => 'mimes:jpeg,jpg | max:1000']
                );

                if ($validator->fails()) {
                    return redirect('/leagues/create')->withInput()->withErrors($validator);
                }

                if (Storage::exists('images/leagues/' . $id . '-cover-photo.jpeg')) {
                    Storage::delete('images/leagues/' . $id . '-cover-photo.jpeg');
                }

                if (Storage::exists('images/leagues/' . $id . '-cover-photo.jpg')) {
                    Storage::delete('images/leagues/' . $id . '-cover-photo.jpg');
                }

                Request::file('cover-photo')->storeAs(
                    'images/leagues', $id . '-cover-photo.' . Request::file('cover-photo')->extension()
                );
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function update()
    {
        $id = Request::get('id');
        $name = Request::get('name');

        $league = LeagueController::get($id);

        if ($name !== $league->name) {
            $validator = Validator::make(
                [ 'Name' => $name ],
                [ 'Name' => 'required|string' ]
            );

            if ($validator->fails()) {
                return redirect('/leagues/edit/' . $id)->withInput()->withErrors($validator);
            }

            /**
             * Check if a league with this name already exists
             */
            $check = DB::table('fw_leagues')->where('name', $name)->count();
            $checkResult = ($check === 0) ? true : false;

            $validator = Validator::make(
                [ 'exists' => $checkResult ],
                [ 'exists' => 'accepted' ],
                [ 'exists' => 'There is already a league with this name' ]
            );

            if ($validator->fails()) {
                return redirect('/leagues/edit/' . $id)->withInput()->withErrors($validator);
            }

            DB::table('fw_leagues')
                ->where('id', $id)
                ->update(['name' => $name]);
        }

        LeagueController::saveImages($id);

        return redirect('/leagues/' . $name . '/');
    }

    public static function topLeagues ($limit = 3)
    {
        $sql = DB::select("SELECT league_id, league_name, SUM(points) as points FROM (SELECT fw_leagues.id as league_id, fw_leagues.name as league_name, fw_leagues.runs_from as rfrom, fw_leagues.runs_to as rto, fw_teams.id as team_id, fw_team_players.person_id as pid, (SELECT SUM(direction) / (SELECT COUNT(*) FROM fw_point_types) FROM fw_points WHERE fw_points.person_id = pid AND fw_points.datestamp >= rfrom AND fw_points.datestamp <= rto) as points FROM fw_leagues INNER JOIN fw_leagues_teams ON fw_leagues.id = fw_leagues_teams.league_id INNER JOIN fw_teams ON fw_leagues_teams.team_id = fw_teams.id INNER JOIN fw_team_players ON fw_teams.id = fw_team_players.team_id ORDER BY points DESC) as temp_table GROUP BY league_id ORDER BY points DESC");

        return $sql;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
