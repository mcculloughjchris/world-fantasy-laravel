<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class PointsController extends Controller
{
    public static function getPointTypes()
    {
        $pointTypes = DB::table('fw_point_types')->get();

        return $pointTypes;
    }

    public static function displayPointsDataRow($person = null, $pointsData = null, $voting = false, $story = null)
    {
        if ($person === null ||
                $pointsData === null) {
            return false;
        }

        $pointsData = $pointsData['points'];

        $pointTypes = PointsController::getPointTypes();

        $html = '<tr>';
        $html .= '<td><span class="tag">' . $person->name . ' <a href="/players/' . $person->id . '"><span class="glyphicon glyphicon-eye-open"></span></span></td>';

        for ($x = 0; $x < count($pointTypes); $x += 1) {
            if (!isset($pointsData[$pointTypes[$x]->name])) {
                $p = [];
            } else {
                $p = $pointsData[$pointTypes[$x]->name];
            }

            $points = 0;

            if (!empty($p)) {
                for ($z = 0; $z < count($p); $z += 1) {
                    $points += $p[$z]->direction;
                }
            }

            $html .= '<td>';

            if ($voting === true &&
                    Auth::user()) {
                $html .= '<votingComponent :pointtype="' . $pointTypes[$x]->id . '" :personid="' . $person->id . '"';

                if ($story !== null) {
                    $html .= ' :storyid="' . $story . '"';
                }

                $html .= ' :points="' . $points . '">';
                $html .= '</votingComponent>';
            } else {
                $html .= $points;
            }

            $html .= '</td>';
        }

        $html .= '</tr>';

        return $html;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
