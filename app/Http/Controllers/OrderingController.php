<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderingController extends Controller
{
    public static function build ($arr = array())
    {
    	if (empty($arr)) {
    		return false;
    	}

    	if (!isset($arr['baseURL'])) {
    		return false;
    	}

    	if (!isset($arr['columns']) || empty($arr['columns'])) {
    		return false;
    	}

    	$currentOption = '';

    	if (isset($arr['by']) && strlen($arr['by'])) {
	    	for ($i = 0; $i < count($arr['columns']); $i += 1) {
	    		if ($arr['columns'][$i]['name'] == $arr['by']) {
	    			$currentOption = $arr['columns'][$i]['name'];
	    			break;
	    		}
	    	}
    	}

    	$return = array();

    	for ($i = 0; $i < count($arr['columns']); $i += 1) {
    		$name = $arr['columns'][$i]['name'];
    		if ($name === $currentOption) {
    			$return[$name] = array ();
    			if ($arr['sort'] === 'asc') {
    				$return[$name]['url'] = $arr['baseURL'] . 'sort=desc&by=' . $name;
    				$return[$name]['glyph'] = 'glyphicon glyphicon-sort-by-' . $arr['columns'][$i]['glyph'] . '-alt';
    			} else {
    				$return[$name]['url'] = $arr['baseURL'] . 'sort=asc&by=' . $name;
    				$return[$name]['glyph'] = 'glyphicon glyphicon-sort-by-' . $arr['columns'][$i]['glyph'];
    			}
    		} else {
    			$return[$name]['url'] = $arr['baseURL'] . 'sort=asc&by=' . $name;
    			$return[$name]['glyph'] = 'glyphicon glyphicon-sort';
    		}
    	}

    	return  $return;
    }
}
