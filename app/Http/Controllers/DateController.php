<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DateController extends Controller
{
    /**
     * Converts a unix timestamp to the date format specified by the user
     */
    public static function str ($timestamp)
    {

        /**
         * If the user isn't logged in, use this default date format
         */
        if (!Auth::user()) {
            $time_display = 'mm/dd/yyyy';
        /**
         * The user is logged in, get the date format set by the user
         */
        } else {
    	   $time_display = Auth::user()->time_display(); 
        }


        /**
         * Search for these
         */
    	$search = array(
    		'mm',
    		'dd',
    		'yyyy'
    	);

        /**
         * Replace with these so PHP understands
         */
    	$replace = array(
    		'm',
    		'd',
    		'Y'
    	);

        /**
         * Do the replacment for PHP's sake
         */
    	$time_display = str_replace($search, $replace, $time_display);

        /**
         * Return the date
         */
    	return date($time_display, $timestamp);
    }
}
