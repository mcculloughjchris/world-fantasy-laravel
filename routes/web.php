<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Homepage
 */
Route::get('/', function () {
    return view('index', array(
    	'stories' => App\Http\Controllers\StoryController::getAll(array(
    		'limit' => 3
    	)),
    	'leagues' => App\Http\Controllers\LeagueController::getAll(array(
    		'limit' => 3
    	))
    ));
});

/**
 * Register page
 */
Route::get('/register', function () {
	return view('register');
});

/**
 * Login page
 */
Route::get('/login', function () {
	return view('login');
});

/**
 * Account edit page
 */
Route::get('/account', ['as' => 'account', function () {
	if (count(Auth::user()) === 0) {
		return redirect('/');
	}
	return view('account');
}]);

/**
 * Account edit page -- Save
 * [Still needs image handling]
 */
Route::post('/account', function () {
	/**
	 * Gather variables
	 */

	$email = Request::get('email');
	$currentPassword = Request::get('current-password');
	$password1 = Request::get('password1');
	$password2 = Request::get('password2');
	$timeDisplay = Request::get('time-display');

	/**
	 * Setup validator to validate time
	 */
	$validator = Validator::make(
		[
			'Time Display' => $timeDisplay,
		],
		[
			'Time Display' => 'required|string',
		]
	);

	/**
	 * If the time validation fails, redirect with errors
	 */
	if ($validator->fails()) {
		return redirect('/account')->withInput()->withErrors($validator);
	}

	/**
	 * Get the user ID
	 */
	$uid = Auth::user()->id;

	/**
	 * Save the time display setting for the user
	 */
	DB::table('users')->where('id', $uid)->update(['time_display' => $timeDisplay]);

	/**
	 * If there's a file, store it locally
	 */
	if (!empty(Request::allFiles())) {

		if (Request::file('avatar')) {
			$validator = Validator::make(
				['Avatar' => Request::file('avatar')],
				['Avatar' => 'mimes:jpeg,jpg | max:1000']
			);

			if ($validator->fails()) {
				return redirect('/account')->withInput()->withErrors($validator);
			}

			/**
			 * Delete the file in case it exists
			 */
			if (Storage::exists('images/avatars/' . Auth::user()->id . '.jpeg')) {
				Storage::delete('images/avatars/' . Auth::user()->id . '.jpeg');
			}

			if (Storage::exists('images/avatars/' . Auth::user()->id . '.jpg')) {
				Storage::delete('images/avatars/' . Auth::user()->id . '.jpg');
			}

			/**
			 * Save the new file
			 */
			Request::file('avatar')->storeAs(
				'images/avatars', Auth::user()->id . '.' . Request::file('avatar')->extension()
			);
		}

		if (Request::file('cover-photo')) {
			$validator = Validator::make(
				['Cover Photo' => Request::file('cover-photo')],
				['Cover Photo' => 'mimes:jpeg,jpg | max:1000']
			);

			if ($validator->fails()) {
				return redirect('/account')->withInput()->withErrors($validator);
			}

			if (Storage::exists('images/avatars/' . Auth::user()->id . '-cover-photo.jpeg')) {
				Storage::delete('images/avatars/' . Auth::user()->id . '-cover-photo.jpeg');
			}

			if (Storage::exists('images/avatars/' . Auth::user()->id . '-cover-photo.jpg')) {
				Storage::delete('images/avatars/' . Auth::user()->id . '-cover-photo.jpg');
			}

			Request::file('cover-photo')->storeAs(
				'images/avatars', Auth::user()->id . '-cover-photo.' . Request::file('cover-photo')->extension()
			);
		}
	}

	/**
	 * If the user wants to update their email address
	 */
	if (strlen($email) > 0) {

		/**
		 * Query the DB to see if this email already exists
		 */
		$emailExists = DB::table('users')->where('email', $email)->count();

		/**
		 * Setup the validator
		 */
		$validator = Validator::make(
			['Email' => $emailExists],
			['Email' => 'not_in:1']
		);

		/**
		 * If the validation fails, redirect with errors
		 */
		if ($validator->fails()) {
			return redirect('/account')->withInput()->withErrors($validator);
		}
		
		/**
		 * Validation didn't fail, save the new email address
		 */
		DB::table('users')->where('id', $uid)->update(['email' => $email]);
	}

	/**
	 * If the user input their current password
	 */
	if (strlen($currentPassword) > 0) {
		/**
		 * Setup the validator to check if this is actually the user's password
		 */
		$validPassword = Hash::check($currentPassword, Auth::user()->password);
		$validator = Validator::make(
			['Password' => $validPassword],
			['Password' => 'in:1'],
			['Password' => 'Invalid password']
		);

		/**
		 * If the validator fails, redirect with errors
		 */
		if ($validator->fails()) {
			return redirect('/account')->withInput()->withErrors($validator);
		}

		/**
		 * The validator didn't fail:
		 * Check if the user input a new password
		 */
		if (strlen($password1)) {
			/**
			 * Setup the validator to check if the two passwords are the same
			 */
			$validPassword = strlen($password1) > 0 && strlen($password2) > 2 && $password1 === $password2;
			$validator = Validator::make(
				['Password' => $validPassword],
				['Password' => 'not_in:1'],
				['Password' => 'new passwords did not match']
			);

			/**
			 * If the validator fails, redirect with errors
			 */
			if ($validator->fails()) {
				return redirect('/account')->withInput()->withErrors();
			}

			/**
			 * The validator didn't fail, save the new password
			 */
			DB::table('users')->where('id', $uid)->update(['password' => Hash::make($password1)]);
		}
	}

	/**
	 * We made it this far, redirect back to the account settings page with the success message
	 */
	return redirect('/account')->with('success', 'Info saved!');

});

/**
 * Stories pages
 */
Route::get('/stories/{id?}', function ($id = null) {
	/**
	 * If the ID is null, the user is requesting the index page
	 */
	if ($id === null) {
		return App\Http\Controllers\StoryController::index();
	}

	/**
	 * The ID is not null, the user is requesting a story's view page
	 */
	$id = intval($id);
	return App\Http\Controllers\StoryController::show($id);
});

/**
 * Leagues pages
 */
Route::get('/leagues/{name?}/{id?}', function ($name = null, $id = null) {
	/**
	 * If the name is null, the user is requesting the index page
	 */
	if ($name === null) {
		return App\Http\Controllers\LeagueController::index();
	/**
	 * If the name is 'join', the user wants to join a league
	 */
	} else if ($name === 'join') {
		/**
		 * If the ID is null, the user is trying to do something funky, ignore them
		 * [Might want to make an error page]
		 */
		if ($id === null) {
			return false;
		}
		/**
		 * Display the join league form
		 */
		return App\Http\Controllers\LeagueController::join($id);
	/**
	 * if the name is 'create', the user wants to make a league
	 */
	} else if ($name === 'create') {
		return view('create-league');
	} else if ($name === 'edit') {
		return view('create-league', array(
			'id' => $id
		));
	}
	/**
	 * The name isn't join or create, so the user wants to see a leagues's view page
	 */
	return App\Http\Controllers\LeagueController::show($name);
});

/**
 * Profile page
 */
Route::get('/profile/{id?}', function ($id = null) {
	/**
	 * If the user isn't logged in, redirect them back to the login page
	 */
	if (!Auth::user()) {
		redirect('/login');
	}

	if ($id === null) {
		return view('profile');
	}

	else {
		$name = DB::table('users')
			->select('name')
			->where('id', $id)
			->limit(1)
			->get();
		if (!empty($name)) {
			$name = $name[0]->name;
			return view('profile', array(
				'id' => $id,
				'name' => $name
			));
		}

		// need an error here
		return false;
	}

});

/**
 * Save a new league
 */
Route::post('/leagues/create', function () {
	return App\Http\Controllers\LeagueController::save();
});

/**
 * Join a league
 */
Route::post('/leagues/join', function () {
	return App\Http\Controllers\LeagueController::doJoin();
});

/**
 * Invite users to a league
 */
Route::post('/leagues/invite', function () {
	return App\Http\Controllers\LeagueController::doInvite();
});

/**
 * Player page
 */
Route::get('/players/{id?}', function ($id = null) {
	if ($id === null) {
		return false;
	}
	return view('player', array(
		'person' => App\Http\Controllers\PersonController::get($id),
		'stories' => App\Http\Controllers\PersonController::getStories($id)
	));
});

/**
 * My teams page
 */
Route::get('/my-teams/', function () {
	return view('teams', array(
		'teams' => App\Http\Controllers\TeamsController::getUserTeams(Auth::user()->id)
	));
});

/**
 * Create team page
 */
Route::get('/my-teams/create', function () {
	return view('create-team', array(
		'peopleList' => App\Http\Controllers\PersonController::index('array')
	));
});

/**
 * Save team
 */
Route::post('/my-teams/create', function () {
	$name = Request::get('name');
	$people = json_decode(Request::get('selected-members'));

	$id = DB::table('fw_teams')->insertGetId([
		'user_id' => Auth::user()->id,
		'name' => $name,
		'datestamp' => time()
	]);

	for ($i = 0; $i < count($people); $i += 1) {
		if ($i === 8) {
			return false;
		}

		DB::table('fw_team_players')->insert([
			'team_id' => $id,
			'person_id' => $people[$i]->id,
			'datestamp' => time()
		]);

	}

	return redirect('/my-teams/');
});

/**
 * My leagues page
 */
Route::get('/my-leagues', function () {
	return view('my-leagues');
});

/**
 * My friends page
 */
Route::get('/my-friends', function () {
	return view('my-friends');
});

/**
 * Buy credits page
 */
Route::get('/buy-credits/', function () {
	return view('buy-credits');
});

/**
 * Purchases credits
 */
Route::post('/buy-credits/', function () {
	if (!Auth::user()) {
		return redirect('/')->with('error', 'You need to be logged in to do that.');
	}

	$id = Auth::user()->id;

	$credits = intval(Request::get('credits'));
	if ($credits === 0) {
		return false;
	}

	$check = DB::table('fw_credits')
		->where('uid', $id)
		->count();

	if ($check === 0) {
		DB::table('fw_credits')
			->insert([
				['uid' => $id, 'credits' => $credits, 'datestamp' => time()]
			]);
	} else {
		DB::table('fw_credits')
			->where('uid', $id)
			->increment('credits', $credits);
	}

	$msg = 'Successfully purchased ' . $credits . ' credit';
	if ($credits !== 1) {
		$msg .= 's';
	}
	$msg .= '.';

	return redirect('/')->with('success', $msg);
});

/**
 * "API" code, mostly in place for AJAX requests,
 * might limit this further later on
 */

/**
 * Gets list of people
 */
Route::get('/api/people-list', function () {
	echo json_encode(App\Http\Controllers\PersonController::index());
});

/**
 * Gets players scores from story
 */
Route::get('/api/homepage/story-to-players/{id}', function($id = null) {
	if ($id === null) {
		return false;
	}

	return json_encode(App\Http\Controllers\StoryController::getPeople($id));
});

/**
 * Gets list of friends
 */
Route::get('/api/friends-list/{id}', function ($id = null) {
	if ($id === null) {
		return false;
	}

	$id = intval($id);
	echo json_encode(App\Http\Controllers\ProfileController::friends($id));
});

/**
 * Saves vote
 */
Route::get('/api/vote/{storyid}/{personid}/{pointtypeid}/{direction}', function ($storyid = null, $personid = null, $pointtypeid = null, $direction = null) {
	if (!Auth::user()) {
		return false;
	}

	$uid = Auth::user()->id;

	if ($storyid === null ||
			$personid === null ||
			$pointtypeid === null ||
			$direction === null) {
		return false;
	}

	$storyid = intval($storyid);
	$personid = intval($personid);
	$pointtypeid = intval($pointtypeid);

	// check if they already voted here
	$count = DB::table('fw_points')
		->where([
			['news_id', '=', $storyid],
			['person_id', '=', $personid],
			['point_type_id', '=', $pointtypeid]
		])
		->count();

	if ($count === 1) {
		DB::table('fw_points')
			->where([
				['user_id', '=', $uid],
				['news_id', '=', $storyid],
				['person_id', '=', $personid],
				['point_type_id', '=', $pointtypeid]
			])
			->update(['direction' => $direction]);
	} elseif ($count === 0) {
		DB::table('fw_points')
			->insert(['user_id' => $uid, 'news_id' => $storyid, 'person_id' => $personid, 'point_type_id' => $pointtypeid, 'direction' => $direction, 'datestamp' => time()]);
	}

	$points = DB::table('fw_points')
		->select('direction')
		->where([
			['news_id', '=', $storyid],
			['person_id', '=', $personid],
			['point_type_id', '=', $pointtypeid]
		])
		->get();

	$totalPoints = 0;
	foreach($points as $point) {
		$totalPoints += $point->direction;
	}

	return $totalPoints;

});

/**
 * Gets notifications for user
 */
Route::get('/api/notifications', function () {
	echo App\Http\Controllers\NotificationsController::displayNotifications();
});

/**
 * Registers a seen notification in the database
 */
Route::get('/api/notifications/see', function () {
	$id = Request::get('id');

	if (!Auth::user()) {
		return false;
	}

	$uid = Auth::user()->id;

	DB::table('fw_notifications')
		->where([
			['to_id', '=', $uid],
			['id', '=', $id]
		])
		->update(['seen' => '1']);

	echo true;
});

/**
 * Deletes a notification
 */
Route::get('/api/notifications/delete', function () {
	$id = Request::get('id');

	if (!Auth::user()) {
		return false;
	}

	$uid = Auth::user()->id;

	DB::table('fw_notifications')
		->where([
			['to_id', '=', $uid],
			['id', '=', $id]
		])
		->delete();

	echo true;
});

Route::get('/api/friendship', function () {
	if (!Request::get('action') && !Request::get('to')) {
		echo false;
		return false;
	}

	if (!Auth::user()) {
		echo false;
		return false;
	}

	$uid = Auth::user()->id;
	$action = Request::get('action');
	$to = Request::get('to');

	if ($action === 'add') {
		// check if the user already added/requested this person
		$check = DB::table('fw_friends')
			->where([
				['user_a', '=', $uid],
				['user_b', '=', $to]
			])
			->get();

		if (count($check) !== 0) {
			if ($check[0]->approved === 0) {
				return response()->json([
					'msg' => 'This user has not approved your request yet.',
					'type' => 'danger'
				]);
			} else if ($check[0]->approved === -1) {
				return response()->json([
					'msg' => 'This user denied your friendship request.',
					'type' => 'danger'
				]);
			}
			return response()->json([
				'msg' => 'You are already friends with this user.',
				'type' => 'danger'
			]);
		}

		DB::table('fw_friends')
			->insert([
				'user_a' => $uid,
				'user_b' => $to,
				'approved' => 0,
				'datestamp' => time()
			]);

		App\Http\Controllers\NotificationsController::saveNotification('frnd_request', $to, $uid);

		return response()->json([
			'msg' => 'Request sent!',
			'type' => 'success'
		]);
	} else if ($action === 'deny') {
		// find the friendship request
		$check = DB::table('fw_friends')
			->where([
				['user_a', '=', $to],
				['user_b', '=', $uid],
				['approved', '=', 0]
			])
			->get();

		if (count($check) !== 0) {
			if ($check[0]->approved === 0) {
				DB::table('fw_friends')
					->where([
						['user_a', '=', $to],
						['user_b', '=', $uid],
						['approved', '=', 0]
					])
					->update(['approved' => -1]);
				return response()->json([
					'success' => 'User denied.'
				]);
			}
		}
	} else if ($action === 'accept') {
		// find the friendship request
		$check = DB::table('fw_friends')
			->where([
				['user_a', '=', $to],
				['user_b', '=', $uid],
				['approved', '=', 0]
			])
			->get();

		if (count($check) !== 0) {
			if ($check[0]->approved === 0) {
				DB::table('fw_friends')
					->where([
						['user_a', '=', $to],
						['user_b', '=', $uid],
						['approved', '=', 0]
					])
					->update(['approved' => 1]);
				return response()->json([
					'success' => 'User accepted.'
				]);
			}
		}
	} else if ($action === 'cancel') {
		$check = DB::table('fw_friends')
			->where([
				['user_a', '=', $uid],
				['user_b', '=', $to],
				['approved', '=', 0]
			])
			->get();

		if (count($check) !== 0) {
			if ($check[0]->approved === 0) {
				DB::table('fw_friends')
					->where([
						['user_a', '=', $uid],
						['user_b', '=', $to],
						['approved', '=', 0]
					])
					->delete();

				return response()->json([
					'success' => 'Friend request cancelled'
				]);
			}
		}
	} else if ($action === 'remove') {
		$check = DB::table('fw_friends')
			->where([
				['user_a', '=', $uid],
				['user_b', '=', $to],
				['approved', '=', 1]
			])
			->orWhere([
				['user_a', '=', $to],
				['user_b', '=', $uid],
				['approved', '=', 1]
			])
			->get();

		if (count($check) !== 0) {
			if ($check[0]->approved === 1) {
				DB::table('fw_friends')
					->where([
						['user_a', '=', $uid],
						['user_b', '=', $to],
						['approved', '=', 1]
					])
					->orWhere([
						['user_a', '=', $to],
						['user_b', '=', $uid],
						['approved', '=', 1]
					])
					->delete();

				return response()->json([
					'success' => 'Friend removed.'
				]);
			}
		}
	}
});

Auth::routes();